package org.bitfountain.network.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.bitfountain.gui.client.ClientApp;
import org.bitfountain.gui.notifications.Notification;
import org.bitfountain.math.SingularMatrixException;
import org.bitfountain.math.reedsolomon.decoding.RSNetDecoder;
import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;
import org.bitfountain.network.Task;
import org.bitfountain.network.packets.CancelTransfer;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FileList;
import org.bitfountain.network.packets.FileNotFound;
import org.bitfountain.network.packets.FilePart;
import org.bitfountain.network.packets.FileRequest;
import org.bitfountain.network.packets.Packet;
import org.bitfountain.network.server.ServerCommunication;

/**
 * Class containing all useful methods to transmit packages
 * to the server.
 * 
 * @author Rivo Vasta
 */
public class ClientCommunication {

	/** Maximum number of packets we handle in one step. */
	private static final int MAX_PACKETS_PER_STEP = 64;

	/** The username of our client. */
	final String name;
	
	/** The socket used to communicate with this client. */
	Socket socket;
	
	/** The client application we're working for. */
	final ClientApp clientApp;
	
	/** Input stream used to read data from the server. */
	public DataInputStream inputStream;
	
	/** Output stream used to write data to the server. */
	public DataOutputStream outputStream;
	
	/** Files being transferred, are with the help of an RSNetEncoder. */
	private List<Task> tasks = new ArrayList<Task>();

	/** Decoders are used to receive files. We poke them from time to time with packets. */
	private Map<Long, RSNetDecoder> decoders = new HashMap<Long, RSNetDecoder>();
	
	/**
	 * Create a new client with a specified name.
	 *
	 * @param pClientApp The client application we're working for
	 * @param pName The name of the client
	 */
	public ClientCommunication(final ClientApp pClientApp, final String pName) {
		this.clientApp = pClientApp;
		this.name = pName;
	}
	
	/**
	 * Attempt a connection with the server.
	 * 
	 * @param host The host to connect to
	 * @param port The port to connect to
	 * 
	 * @throws IOException in case we can't reach the server
	 */
	public final void connectToServer(final String host, final int port) throws IOException {
		
		try {
			socket = new Socket(host, port);
			inputStream = new DataInputStream(socket.getInputStream());
			outputStream = new DataOutputStream(socket.getOutputStream());
			ack();
		} catch (IOException e) {
			throw new IOException("Échec de la connexion à " + host + ":" + port);
		}
		
	}

	/**
	 * Send acknowledgement packets to the server. 
	 * 
	 * @throws IOException In case anything I/O-related goes wrong (O, RLY?)
	 */
	private void ack() throws IOException {
		
		// send magic number
		outputStream.writeInt(ServerCommunication.MAGIC_NUMBER);
		
		// send client name
		byte[] rawName = name.getBytes();
		outputStream.writeInt(rawName.length);
		outputStream.write(rawName);
		
		outputStream.flush();
		
	}
	
	/**
	 * Send a file to the server via the network.
	 * 
	 * @param file The file to be sent.
	 * 
	 * @return RSNetEncoder the encoder used to encode this file.
	 */
	public final RSNetEncoder sendFile(final File file) {
		
		try {
			RSNetEncoder encoder = new RSNetEncoder(file, outputStream);
			synchronized (tasks) {
				tasks.add(encoder);
			}
			return encoder;
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("I/O error");
		}
		
	}
	
	/**
	 * Receive a file from the network.
	 * 
	 * @param header Header of the file to be received
	 * @param destination Where to save the decoded file
	 * 
	 * @return RSNetDecoder the decoder used to decode this file.
	 */
	@SuppressWarnings("boxing")
	public final RSNetDecoder receiveFile(final FileHeader header, final File destination) {
	
		try {
			new FileRequest(header.getFileId()).write(outputStream);
			synchronized (decoders) {
				RSNetDecoder decoder = new RSNetDecoder(header, destination);
				decoders.put(header.getFileId(), decoder);
				return decoder;
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Failed to receive an encoded file from the network.");
		}
		
	}
	
	/**
	 * Send a request to cancel a current download.
	 * 
	 * @param header Header of the receiving file to be cancelled
	 */
	public final void cancelDownload(final FileHeader header) {
		try {
			new CancelTransfer(header.getFileId()).write(outputStream);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Failed to cancel transfer");
		}
	}
	
	/**
	 * Stop the upload.
	 * 
	 * @param header Header of the uploading file to be cancelled
	 */
	public final void cancelUpload(final FileHeader header) {
		synchronized (tasks) {
			Iterator<Task> iter = tasks.iterator();
			
			long headerFileID = header.getFileId();
			while (iter.hasNext()) {
				Task iterNext = iter.next();
				if (iterNext instanceof RSNetEncoder) {
					long candidateFileID = ((RSNetEncoder) iterNext).getHeader().getFileId();
					if (headerFileID == candidateFileID) {
						iter.remove();
					}
				}
				
			}
		}
	}
	
	/**
	 * Step all tasks.
	 * 
	 * @throws IOException Well, we're on a network, after all.
	 */
	public final void step() throws IOException {
		
		int maxPackets = MAX_PACKETS_PER_STEP;
		while (inputStream.available() >= 1 && maxPackets-- > 0) {
			Packet packet = Packet.readPacket(inputStream);
			handlePacket(packet);
		}
		
		synchronized (tasks) {
			Iterator<Task> iter = tasks.iterator();
			while (iter.hasNext()) {
				try {
					Task task = iter.next();
					if (!task.step()) {
						if (task instanceof RSNetEncoder) {
							String finishedName = ((RSNetEncoder) task).getHeader().getFileName();
							Notification.show("Téléversement de " + finishedName + " terminé.");
						}
						iter.remove();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * Handle an incoming packet from the server.
	 * 
	 * @param packet The received packet.
	 * 
	 * @throws IOException In case something goes awwwwwwfully wrong.
	 */
	@SuppressWarnings("boxing")
	private void handlePacket(final Packet packet) throws IOException {
		
		if (packet instanceof FilePart) {
			FilePart filePart = (FilePart) packet;
			synchronized (decoders) {
				RSNetDecoder decoder = decoders.get(filePart.getFileId());
				if (decoder == null) {
					System.out.println("Ignoring packet with fileId " + filePart.getFileId() + ", we're not currently receiving such a file.");
				} else {
					try {
						if (!decoder.step(filePart)) {
							Notification.show("Téléchargement de " + decoder.getHeader().getFileName() + " terminé.");
							decoders.remove(filePart.getFileId());
						}
					} catch (SingularMatrixException e) {
						decoders.remove(filePart.getFileId());
						decoder.getDestination().delete(); // don't leave dirty files.
						
						Object[] options = { "Réessayer", "Annuler" };
						int result = JOptionPane.showOptionDialog(null, "Le transfert du fichier "
								+ decoder.getHeader().getFileName()
								+ " a échoué car la qualité de transmission était trop mauvaise. Réessayer?",
								"Échec de la (trans)mission",
								JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
						if (result == JOptionPane.OK_OPTION) {
							receiveFile(decoder.getHeader(), decoder.getDestination());
						}
					}
				}
			}
		} else if (packet instanceof FileList) {
			clientApp.getMainWindow().getEventHandler().gotFileList((FileList) packet);
		} else if (packet instanceof FileNotFound) {
			JOptionPane.showMessageDialog(null,
					"Le fichier demandé n’est plus disponible sur le serveur.\n"
					+ "Auriez-vous tenté de télécharger un fichier sans rafraichir la liste auparavant ?",
					"Erreur : fichier non disponible", JOptionPane.WARNING_MESSAGE);
		} else {
			System.out.println("Client received unexpected packet type " + packet.getClass().getSimpleName());
		}
		
	}

	/**
	 * Disconnect from the server. Any attempt to send a file after
	 * calling that method will result in an error.
	 * @throws IOException 
	 */
	public final void disconnect() throws IOException {
		socket.close();
	}
	
	/**
	 * Returns the name of the client.
	 * @return the name of the client
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * @return true if the client is idle, false if it's busy doing some
	 * transfers.
	 */
	public final boolean isIdle() {
		try {
			return tasks.isEmpty() && inputStream.available() == 0;
		} catch (IOException e) {
			return false;
		}
	}
	
}

