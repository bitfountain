package org.bitfountain.network.server;

import java.util.ArrayList;
import java.util.List;

/**
 * Server application core. 
 * 
 * @author Rivo Vasta
 * @author Amos Wenger
 */
public class Server {
	
	/** List of clients that are connected to the server. */
	private List<ClientInfo> clients = new ArrayList<ClientInfo>();
	
	/**
	 * Creates a new Server.
	 */
	public Server() {
		// nothing to do here.
	}
	
	/**
	 * Adds a new client to the list of clients.
	 * 
	 * @param client the new client
	 */
	public final void addClient(final ClientInfo client) {
		synchronized (clients) {
			clients.add(client);
		}
	}
	
	/**
	 * @return a list of clients that are connected
	 */
	public final List<ClientInfo> getClients() {
		return clients;
	}
}

