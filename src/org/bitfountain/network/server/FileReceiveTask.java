package org.bitfountain.network.server;

import java.util.ArrayList;
import java.util.List;

import org.bitfountain.math.reedsolomon.RSAlgorithm;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FilePart;

/**
 *
 * 
 * @author Amos Wenger
 */
public class FileReceiveTask {

	/** Header info about the file we're receiving. */
	private FileHeader header;
	
	/** Number of the last packet (ie. how we know a transfer has ended) */
	private long lastPacketNum;
	
	/** Buffer where we store the parts received from the client. */
	private List<FilePart> parts = new ArrayList<FilePart>();
	
	/** Clients that were connected when this upload had started. */
	private List<ClientInfo> clients = new ArrayList<ClientInfo>();
	
	/**
	 * Create a new file receive task.
	 * @param pHeader Header info about the file we're receiving
	 * @param connectedClients Clients that were connected when this upload had started
	 */
	public FileReceiveTask(final FileHeader pHeader, final List<ClientInfo> connectedClients) {
		this.header = pHeader;
		this.clients.addAll(connectedClients);
		this.lastPacketNum = -1 + pHeader.numPackets(RSAlgorithm.DEFAULT_DATA_BLOCK_SIZE, RSAlgorithm.DEFAULT_TOTAL_BLOCK_SIZE);
	}

	/**
	 * Handle an incoming filePart packet and store it in the buffer.
	 * @param filePart The file part packet.
	 * @return true if we need more data, false if we're done.
	 */
	public final boolean step(final FilePart filePart) {
		parts.add(filePart);
		
		// as soon as we receive a filePart with a packet number equal
		// to expectedPackets, we are done receiving the file =)
		return filePart.getPacketNumber() != lastPacketNum;
	}
	
	/**
	 * @return file parts buffer.
	 */
	public final List<FilePart> getParts() {
		return parts;
	}
	
	/**
	 * @return list of clients this file will be accessible to.
	 */
	public final List<ClientInfo> getClients() {
		return clients;
	}

	/**
	 * @return header information about the file being received.
	 */
	public final FileHeader getHeader() {
		return header;
	}
	
}
