package org.bitfountain.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import org.bitfountain.io.Logger;
import org.bitfountain.network.Task;
import org.bitfountain.network.packets.CancelTransfer;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FileNotFound;
import org.bitfountain.network.packets.FilePart;
import org.bitfountain.network.packets.FileRequest;
import org.bitfountain.network.packets.Packet;

/**
 * Class containing all useful methods to communicate
 * with the clients.
 * 
 * @author Rivo Vasta
 */
public class ServerCommunication {

	/** Maximum number of packets we handle in one step. */
	private static final int MAX_PACKETS_PER_STEP = 2048;
	
	/** Default port on which to listen. */
	public static final int DEFAULT_PORT = 23987;
	
	/** Magic number a client must send to the server before doing anything else. */
	public static final int MAGIC_NUMBER = 0xDEADBABE;
	
	/** Port on which to listen. */
	int port;
	
	/** Percentage of packets lost when transmitting FileParts back to clients. */
	double loss = 0;
	
	/** Server instance we're handling the communication of. */
	Server server;

	/** For logging messages (obviously). */
	Logger logger;
	
	/** All our tasks, such as sending files to the server. */
	List<Task> tasks = new ArrayList<Task>();
	
	/** Files we're currently receiving (map the fileId to a FileReceiveTask). */
	HashMap<Long, FileReceiveTask> receives = new HashMap<Long, FileReceiveTask>();
	
	/**
	 * Create a new ServerCommunication.
	 * 
	 * @param pLogger where to display our messages.
	 * 
	 * @throws IOException In case something goes very wrong.
	 */
	public ServerCommunication(final Logger pLogger) throws IOException {
		this.port = DEFAULT_PORT;
		this.logger = pLogger;
		this.server = new Server();
		
		new Thread(new ClientListener()).start();
		new Thread(new ClientPoller()).start();
	}
	
	/**
	 * Is listening to any new client who would connect
	 * to the server.
	 * @author Rivo Vasta
	 */
	class ClientListener implements Runnable {

		@Override
		public void run() {

			logger.log("Listening for connections on port " + port);
			
			// Accepts a new client at any time.
			ServerSocket serverSocket;
			try {
				serverSocket = new ServerSocket(port);
				while (!serverSocket.isClosed()) {
					Socket socket = serverSocket.accept();
					ClientInfo newClient = new ClientInfo("<clientName>", socket);
	
					int magicNumber = newClient.getInputStream().readInt();
					if (magicNumber != MAGIC_NUMBER) {
						logger.log("Incompatible protocol from client at "
								+ socket.getInetAddress().getHostAddress() + ", kicking.");
						socket.close();
						continue;
					}
					
					int nameLength = newClient.getInputStream().readInt();
					byte[] nameRaw = new byte[nameLength];
					newClient.getInputStream().readFully(nameRaw);
					newClient.setName(new String(nameRaw));
					
					logger.log(newClient.getName() + " joined.");
					server.addClient(newClient);
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Couldn't start server on port " + port + "! Is it already running?");
				System.exit(1);
			}
			
		}
	}
	
	/**
	 * Polls clients for packets, and handles them consequently.
	 * @author Amos Wenger
	 */
	class ClientPoller implements Runnable {
		
		/**
		 * Main loop of the client poller.
		 */
		public void run() {
			
			while (true) {
				
				boolean idle = true;
				
				synchronized (server.getClients()) {
					Iterator<ClientInfo> iter = server.getClients().iterator();
					while (iter.hasNext()) {
						ClientInfo client = iter.next();
						try {
							synchronized (client.getInputStream()) {
								int maxPackets = MAX_PACKETS_PER_STEP;
								while (client.getInputStream().available() >= 1 && maxPackets-- > 0) {
									idle = false;
									Packet packet = Packet.readPacket(client.getInputStream());
									handlePacket(client, packet);
								}
							}
						} catch (IOException e) {
							// IOException encompasses, among others, EOFException and SocketException.
							logger.log(client.getName() + " disconnected.");
							iter.remove();
						}
					}
				}
				
				synchronized (tasks) {
					Iterator<Task> iter = tasks.iterator();
					while (iter.hasNext()) {
						try {
							Task task = iter.next();
							if (!task.step()) {
								iter.remove();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			
				// don't hog CPU if nothing needs to be done.
				if (tasks.isEmpty() && idle) {
					try {
						Thread.sleep(200L);
					} catch (Exception e) { e.printStackTrace(); }
				}
			
			}
			
		}

		/**
		 * Handle a packet received from a client.
		 * 
		 * @param client The client from which we received it.
		 * @param packet The packet received
		 * @throws IOException if any I/O problem occurs.
		 */
		@SuppressWarnings("boxing")
		private void handlePacket(final ClientInfo client, final Packet packet) throws IOException {
			
			if (packet instanceof FilePart) {
				
				FilePart filePart = (FilePart) packet;
				
				FileReceiveTask task = receives.get(filePart.getFileId());
				if (task != null) {
					if (!task.step(filePart)) {
						// store the file part into the buffer of all connected clients.
						for (ClientInfo receiver : task.getClients()) {
							if (server.getClients().contains(receiver)) {
								receiver.getFileList().add(task.getHeader());
								receiver.getBuffer().addAll(task.getParts());
								receiver.getFileList().write(receiver.getOutputStream());
							}
						}
					}
				}
				
				
			} else if (packet instanceof FileHeader) {
				
				FileHeader fileHeader = (FileHeader) packet;
				
				// store the file header into the list of all connected clients.
				receives.put(fileHeader.getFileId(), new FileReceiveTask(fileHeader, server.getClients()));
				logger.log(client.getName() + " sends " + fileHeader);
				
			} else if (packet instanceof FileRequest) {
				
				FileRequest request = (FileRequest) packet;
				
				FileHeader header = null;
				for (FileHeader candidate : client.getFileList()) {
					if (candidate.getFileId() == request.getFileId()) {
						header = candidate;
						break;
					}
				}
				
				if (header == null) {
					
					new FileNotFound(request.getFileId()).write(client.getOutputStream());
					
				} else {
				
					logger.log(client.getName() + " receives " + header);

					List<FilePart> parts = new ArrayList<FilePart>();
					for (FilePart part : client.getBuffer()) {
						if (part.getFileId() != request.getFileId()) {
							continue; // skip file parts that are irrelevant 
						}
						
						parts.add(part);
					}
					
					// introduce lossage
					int lostPackets = (int) (loss / 100.0 * parts.size());
					Random rand = new Random();
					for (int i = 0; i < lostPackets; i++) {
						// never remove the last packet: we need it (hence the '- 1')
						// that's cheating, but no more than making all the other
						// types of packet reliable =)
						parts.remove(rand.nextInt(parts.size() - 1));
					}
					
					tasks.add(new FileSendTask(parts, client));
				
				}
				
			} else if (packet instanceof CancelTransfer) {
				
				CancelTransfer cancel = (CancelTransfer) packet;
				
				FileHeader header = null;
				for (FileHeader candidate : client.getFileList()) {
					if (candidate.getFileId() == cancel.getFileId()) {
						header = candidate;
						break;
					}
				}
				
				if (header == null) {
					new FileNotFound(cancel.getFileId()).write(client.getOutputStream());
				} else {
					logger.log(client.getName() + " wants to cancel transfer of file  " + header);
					
					Iterator<Task> iter = tasks.iterator();
					
					long headerFileID = header.getFileId();
					while (iter.hasNext()) {
						long candidateFileID = ((FileSendTask) iter.next()).getFirstFilePart().getFileId();
						if (headerFileID == candidateFileID) {
							iter.remove();
						}
					}					
				}
			} else {
				
				logger.log("Got unknown packet type " + packet.getClass().getSimpleName());
				
			}
			
		}
	
	}

	/**
	 * @param pLoss Set the percentage of loss the server has to simulate.
	 */
	public final void setLoss(final double pLoss) {
		this.loss = pLoss > 13.0 ? 13.0 : pLoss;
	}
}

