package org.bitfountain.network.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.bitfountain.network.packets.FileList;
import org.bitfountain.network.packets.FilePart;

/**
 * Information the server holds about a particular client.
 * 
 * @author Rivo Vasta
 * @author Amos Wenger
 */
public class ClientInfo {
	
	/** Name of the client. */
	private String name;
	
	/** Socket used to communicate with it. */
	private Socket socket;
	
	/** Input stream used to read data from the client. */
	private DataInputStream inputStream;
	
	/** Input stream used to write data to the client. */
	private DataOutputStream outputStream;
	
	/** List of files this client should know about. */
	private FileList fileList;
	
	/** Buffer of FilePart received. */
	private List<FilePart> buffer;
	
	/**
	 * Create a new client information.
	 * 
	 * @param pName Name of the client.
	 * @param pSocket Socket used to communicate with the client.
	 * 
	 * @throws IOException when trying to obtain an input stream and an output
	 *  stream from the socket.
	 */
	public ClientInfo(final String pName, final Socket pSocket) throws IOException {
		this.name = pName;
		this.socket = pSocket;
		this.inputStream = new DataInputStream(socket.getInputStream());
		this.outputStream = new DataOutputStream(socket.getOutputStream());
		this.fileList = new FileList();
		this.buffer = new ArrayList<FilePart>();
	}
	
	/**
	 * @return the input stream for this client.
	 */
	public final DataInputStream getInputStream() {
		return inputStream;
	}
	
	/**
	 * @return the output stream to this client.
	 */
	public final DataOutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * @return the name of the client
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * @param pName name to be set
	 */
	public final void setName(final String pName) {
		this.name = pName;
	}
	
	/**
	 * @return the file headers this client should know about.
	 */
	public final FileList getFileList() {
		return fileList;
	}
	
	/**
	 * @return buffer of file parts this client has received,
	 * according to our simulation.
	 */
	public final List<FilePart> getBuffer() {
		return buffer;
	}
	
}
