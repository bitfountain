package org.bitfountain.network.server;

import java.io.IOException;
import java.util.List;

import org.bitfountain.network.Task;
import org.bitfountain.network.packets.FilePart;

/**
 * Task for sending a file from the server to the client.
 * The loss is applied beforehand, ie. the list of parts
 * that we're sending is decided earlier by ServerCommunication
 * 
 * @author Amos Wenger
 */
public class FileSendTask implements Task {
	
	/** File parts we have to send. */
	private List<FilePart> parts;
	
	/** Which part we're currently sending. */
	private int index = 0;

	/** Client we're sending the file to. */
	private ClientInfo destination;
	
	/** true when the task is finished. */
	boolean isFinished;
	
	/**
	 * Create a new file send task.
	 * 
	 * @param pParts Parts to send.
	 * @param pDestination Client we're sending the file to.
	 */
	public FileSendTask(final List<FilePart> pParts, final ClientInfo pDestination) {
		this.parts = pParts;
		this.destination = pDestination;
	}

	@Override
	public final double getProgress() {
		return (double) index / (double) parts.size();
	}
	
	@Override
	public double getSpeed() {
		// we don't need to estimate, server-side
		return -1;
	}

	@Override
	public final boolean step() throws IOException {
		if (index >= parts.size()) {
			isFinished = true;
			return false; // we're done here.
		}
		
		parts.get(index++).write(destination.getOutputStream());
		return true;
	}

	@Override
	public final boolean isFinished() {
		return isFinished;
	}
	
	/**
	 * Give the first filePart of the file beeing sent, to retrieve some useful
	 * informations about it.
	 * @return the first part of the file beeing sent.
	 */
	public final FilePart getFirstFilePart() {
		return parts.get(0);
	}
	
	@Override
	public final String toString() {
		return "Sending file " + parts.get(0).getFileId() + " to " + destination.getName();
	}

}
