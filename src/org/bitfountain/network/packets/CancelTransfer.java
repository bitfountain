package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Injunction to cancel a current transfer.
 * 
 * @author Rivo Vasta
 */
public class CancelTransfer extends Packet {

	/** Identifier of the requested file. */
	private long fileId;
	
	/**
	 * Create a new file request, given a specific file ID.
	 * @param pFileId The ID of the file to cancel the transfer of.
	 */
	public CancelTransfer(final long pFileId) {
		this.fileId = pFileId;
	}
	
	/**
	 * Read a cancel transfer request from an inputstream.
	 * 
	 * @param stream The stream to read from
	 * 
	 * @throws IOException In case any I/O error occurs. 
	 */
	public CancelTransfer(final DataInputStream stream) throws IOException {
		this.fileId = stream.readLong();
	}
	
	@Override
	public final void write(final DataOutputStream stream) throws IOException {
		
		stream.writeByte(PacketType.CANCEL_TRANSFER.ordinal());
		
		stream.writeLong(fileId);
		
	}

	/**
	 * @return the identifier of the file to cancel the transfer of.
	 */
	public final long getFileId() {
		return fileId;
	}
	
}

