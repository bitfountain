package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Response from the server to a client, telling it
 * that a requested file wasn't found.
 * 
 * @author Amos Wenger
 */
public class FileNotFound extends Packet {

	/** Identifier of the file that wasn't found. */
	private long fileId;
	
	/**
	 * Read a FileNotFound packet from stream.
	 * 
	 * @param stream Stream to read the packet from.
	 * 
	 * @throws IOException In case we can't read.
	 */
	public FileNotFound(final DataInputStream stream) throws IOException {
		this.fileId = stream.readLong();
	}

	/**
	 * Create a new file not found message for a given file id.
	 * @param pFileId ID of the file that wasn't found on the server.
	 */
	public FileNotFound(final long pFileId) {
		this.fileId = pFileId;
	}

	@Override
	public final void write(final DataOutputStream stream) throws IOException {
		stream.writeByte(Packet.PacketType.FILE_NOT_FOUND.ordinal());
		stream.writeLong(fileId);
	}
	
	/**
	 * @return the ID of the file that wasn't found.
	 */
	public final long getFileId() {
		return fileId;
	}

}
