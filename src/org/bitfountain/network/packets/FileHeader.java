package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.zip.CRC32;

import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;

/**
 * Info about a file, that can be sent in a list of files.
 * 
 * @author Amos Wenger
 */
public class FileHeader extends Packet {
	
	/** Used to FORMAT file size, etc. */
	static DecimalFormat format = new DecimalFormat("0.00");
	
	/** File id, ie. the CRC-32 of the file name */
	private long fileId;
	
	/** File size, in bytes. */
	private long fileSize;
	
	/** File name. */
	private String fileName;
	
	/**
	 * Create a new Header.
	 * 
	 * @param pFileName The name of the file.
	 * @param pFileSize The size of the file, in bytes.
	 */
	public FileHeader(final String pFileName, final long pFileSize) {
		this.fileName = pFileName;
		this.fileSize = pFileSize;
		computeFileId();
		
		
	}
	
	/**
	 * @param dataBlockSize Number of data bytes we can fit in a block
	 * @param totalBlockSize Total size of a block
	 * 
	 * @return the exact number of packets needed to transfer this file.
	 */
	public final long numPackets(final int dataBlockSize, final int totalBlockSize) {
		
		long expectedPackets = 0;
		
		int numBlocks = (int) (getFileSize() / dataBlockSize + 1);
		while (numBlocks > RSNetEncoder.PACKET_DATA_SIZE) {
			expectedPackets += totalBlockSize;
			numBlocks -= RSNetEncoder.PACKET_DATA_SIZE;
		}
		if (numBlocks > 0) {
			expectedPackets += totalBlockSize;
		}
		
		return expectedPackets;
		
	}
	
	/**
	 * Create a new Header.
	 * 
	 * @param pFile A File (using the Java File class).
	 */
	public FileHeader(final File pFile) {
		this(pFile.getName(), pFile.length());
	}

	/**
	 * Compute the file ID from its name.
	 */
	private void computeFileId() {
		CRC32 crc = new CRC32();
		crc.update(fileName.getBytes());
		this.fileId = crc.getValue();
	}
	
	/**
	 * Decode the header from an inputstream.
	 * 
	 * @param stream The stream to read from
	 * 
	 * @throws IOException In case any I/O error occurs. 
	 */
	public FileHeader(final DataInputStream stream) throws IOException {

		// read file size
		this.fileSize = stream.readLong();
		
		// read the filename's length
		int fileNameLength = stream.readInt();
	
		// read the filename itself
		byte[] fileNameRaw = new byte[fileNameLength];
		stream.readFully(fileNameRaw);
		
		this.fileName = new String(fileNameRaw);
		
		computeFileId();
	}
	
	/**
	 * Write the error to a stream.
	 * 
	 * @param stream The stream to write to.
	 * 
	 * @throws IOException in case any  
	 */
	@Override
	public final void write(final DataOutputStream stream) throws IOException {

		stream.writeByte(PacketType.FILE_HEADER.ordinal());
		
		// write the file size
		stream.writeLong(fileSize);
		
		// write the filename's length
		byte[] fileNameRaw = fileName.getBytes();
		stream.writeInt(fileNameRaw.length);
		
		// write the filename itself
		stream.write(fileNameRaw);
		
	}

	/**
	 * @return the file ID
	 */
	public final long getFileId() {
		return fileId;
	}

	/**
	 * @return the file size
	 */
	public final long getFileSize() {
		return fileSize;
	}

	/**
	 * @return the file name
	 */
	public final String getFileName() {
		return fileName;
	}
	
	@Override
	public final String toString() {
		return "\"" + getFileName() + "\" (" + format.format(getFileSize() / 1024.0)
			+ " Kibi), ID = #" + getFileId();
	}
	
}
