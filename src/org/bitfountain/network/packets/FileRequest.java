package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Packet sent when the client wants to receive + decode a particular file. 
 * 
 * @author Amos Wenger
 */
public class FileRequest extends Packet {

	/** Identifier of the requested file. */
	private long fileId;
	
	/**
	 * Create a new file request, given a specific file ID.
	 * @param pFileId The ID of the requested file.
	 */
	public FileRequest(final long pFileId) {
		this.fileId = pFileId;
	}
	
	/**
	 * Read a File request packet from a data input stream.
	 * 
	 * @param stream Stream to read the packet from
	 * @throws IOException In case anything goes wrong.
	 */
	public FileRequest(final DataInputStream stream) throws IOException {
		this.fileId = stream.readLong();
	}

	@Override
	public final void write(final DataOutputStream stream) throws IOException {
		stream.writeByte(PacketType.FILE_REQUEST.ordinal());
		
		stream.writeLong(fileId);
	}
	
	/**
	 * @return the identifier of the requested file.
	 */
	public final long getFileId() {
		return fileId;
	}

}
