package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * A packet can be sent over the network. First, the type of the packet
 * is sent as an int, and then the packet's content, via its write() method.
 * 
 * On the contrary, the readPacket() method allows to read a packet
 * from an input stream by reading its type, then calling the respective
 * constructor.
 * 
 * @author Amos Wenger
 */
public abstract class Packet {

	/**
	 * Type of the packet to be sent. 
	 * 
	 * @author Amos Wenger
	 */
	public static enum PacketType {
		/** List of the files available. */
		FILE_LIST,
		/** Part of a file (e.g. pack of blocks). */
		FILE_PART,
		/** Information about a file being sent (e.g. name, size) */
		FILE_HEADER,
		/** Client requesting a file to the server. */
		FILE_REQUEST,
		/** Client requesting a file, but it's not on the server. */
		FILE_NOT_FOUND,
		/** A transfer has been cancelled by client. */
		CANCEL_TRANSFER,
	}

	/** Empty constructor to make subclasses' lives easier. Yay Java. */
	public Packet() { }
	
	/**
	 * Constructor to read the packet from a stream, minus its type int.
	 * 
	 * @param stream The stream to read from.
	 */
	public Packet(final DataInputStream stream) { }
	
	/**
	 * Read a packet from an input stream.
	 * 
	 * @param stream The stream to read from.
	 * @return the packet that's been read.
	 * 
	 * @throws IOException In case anything goes wrong I/O-wise.
	 */
	public static final Packet readPacket(final DataInputStream stream) throws IOException {
		byte packetType = stream.readByte();
		
		if (packetType < 0 || packetType >= PacketType.values().length) {
			throw new Error("Unknown packet type: " + packetType + ", network error, aborting.");
		}
		
		switch(PacketType.values()[packetType]) {
			case FILE_LIST:
				return new FileList(stream);
			case FILE_PART:
				return new FilePart(stream);
			case FILE_HEADER:
				return new FileHeader(stream);
			case FILE_REQUEST:
				return new FileRequest(stream);
			case FILE_NOT_FOUND:
				return new FileNotFound(stream);
			case CANCEL_TRANSFER:
				return new CancelTransfer(stream);
			default:
				throw new Error("Protocol error, received unknown packet type " + packetType);
		}
	}
	
	/**
	 * Write this packet to an output stream.
	 * 
	 * @param stream The stream to write to.
	 * 
	 * @throws IOException In case something bad happens.
	 */
	public abstract void write(final DataOutputStream stream) throws IOException;
	
}
