package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Send the list of files with a packet.
 * 
 * @author Amos Wenger
 */
public class FileList extends Packet implements Iterable<FileHeader> {
	
	/** The list of files. */
	private ArrayList<FileHeader> files = new ArrayList<FileHeader>();
	
	/** Create a new empty list of files. */
	public FileList() { }
	
	/**
	 * Read the list of files from a stream.
	 * @param stream The stream from which to read
	 * 
	 * @throws IOException Any kind of I/O problem occured.
	 */
	public FileList(final DataInputStream stream) throws IOException {
		// how many files headers?
		int numList = stream.readInt();
		for (int i = 0; i < numList; i++) {
			int packetType = stream.readByte();
			if (packetType != PacketType.FILE_HEADER.ordinal()) {
				throw new Error("Expected File header packet, got packet type " + packetType + " instead.");
			}
			
			// read file headers one by one
			files.add(new FileHeader(stream));
		}
	}
	
	/**
	 * @return the inner list of files.
	 */
	public final List<FileHeader> getFiles() {
		return files;
	}
	
	@Override
	public final void write(final DataOutputStream stream) throws IOException {
		stream.writeByte(PacketType.FILE_LIST.ordinal());
		
		stream.writeInt(files.size());
		for (FileHeader file : files) {
			// write file header one by one
			file.write(stream);
		}
	}

	/**
	 * Adds a file header to the file list.
	 * @param fileHeader the file header to add to the list.
	 */
	public final void add(final FileHeader fileHeader) {
		getFiles().add(fileHeader);
	}

	@Override
	public final Iterator<FileHeader> iterator() {
		return files.iterator();
	}

}
