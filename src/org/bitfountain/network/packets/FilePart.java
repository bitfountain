package org.bitfountain.network.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;

/**
 * Send part of a file by interlacing blocks into packets.
 * 
 * @author Amos Wenger
 */
public class FilePart extends Packet {

	/**
	 * 1 bytes for the packetType (int)
	 * 8 bytes for the fileId (long),
	 * 4 bytes for the packetNumber (int).
	 */
	public static final int HEADER_SIZE = 1 + 8 + 4;
	
	/**
	 * Unique (hopefully) identifier for a file on the network.
	 * It's a CRC-32 hash of the file path.
	 */
	private long fileId;
	
	/**
	 * Number of the packet for this file.
	 */
	private int packetNumber;
	
	/**
	 * Data of this file block. Columns are blocks, rows are packets.
	 */
	private byte[] blockParts;

	/**
	 * Create a new file block packet.
	 * 
	 * @param fileHeader Info about the file these blocks belong to
	 * @param pPacketNumber number of the packet for this file.
	 * @param pBlockParts Data blocks we're sending 
	 */
	public FilePart(final FileHeader fileHeader, final int pPacketNumber, final byte[] pBlockParts) {
		this.fileId = fileHeader.getFileId();
		this.packetNumber = pPacketNumber;
		this.blockParts = pBlockParts;
	}
	
	/**
	 * Read a FileBlock from a DataInputStream.
	 * 
	 * @param stream to read from
	 * @throws IOException Some sort of I/O error has occured
	 */
	public FilePart(final DataInputStream stream)
			throws IOException {
		
		/** (hopefully) unique file ID */
		this.fileId = stream.readLong();
		
		/** packet number, to tolerate loss */
		this.packetNumber = stream.readInt();
		
		/** interlaced block data */
		this.blockParts = new byte[RSNetEncoder.PACKET_DATA_SIZE];
		stream.readFully(blockParts);
		
	}

	@Override
	public final void write(final DataOutputStream stream) throws IOException {
		
		/** type of the packet */
		stream.writeByte(PacketType.FILE_PART.ordinal());
		
		/** (hopefully) unique file ID */
		stream.writeLong(fileId);
		
		/** packet number, to tolerate loss */
		stream.writeInt(packetNumber);
		
		/** interlaced block data */
		stream.write(blockParts);
		
	}
	
	/**
	 * @return the identifier of the file this part belongs to.
	 */
	public final long getFileId() {
		return fileId;
	}
	
	/**
	 * @return the number of the packet for a given file.
	 */
	public final int getPacketNumber() {
		return packetNumber;
	}
	
	/**
	 * @return the byte data contained in this packet.
	 */
	public final byte[] getBlockParts() {
		return blockParts;
	}

}
