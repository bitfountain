package org.bitfountain.network;

/**
 * Interface for all classes that have a measurable progression.
 * 
 * @author Rivo Vasta
 */
public interface Progressive {
	
	/**
	 * @return estimated progress of this task, on a scale of 0.0 to 1.0
	 * negative values mean that the progress can't really be estimated yet.
	 */
	double getProgress();
	
	/**
	 * @return the operation speed, in Kibi/s
	 */
	double getSpeed();
	
	/**
	 * @return true if the transfer is finished.
	 */
	boolean isFinished();
}

