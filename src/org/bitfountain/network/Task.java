package org.bitfountain.network;

import java.io.IOException;

/**
 * Generic interface for a task.
 * 
 * @author Amos Wenger
 */
public interface Task extends Progressive {

	/**
	 * Do a bit of work.
	 * 
	 * @return true if there's some more work to be done, false if we're finished
	 * @throws IOException 
	 */
	boolean step() throws IOException;
	
}
