/**
 * 
 */
package org.bitfountain.gui.notifications;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Slide-in notification animating sheet.
 * 
 * @author Joshua Marinacci
 * @author Chris Adamson 
 */
class AnimatingSheet extends JPanel {
	
	/** UID for serialization. */
	private static final long serialVersionUID = -2167873288990690576L;
	
	/** Animating size of this sheet. */
	Dimension animatingSize = new Dimension(0, 1);
	
	/** Component we're faking the display of. */
	JComponent source;
	
	/** Off-screen image create from a component's content. */
	BufferedImage offscreenImage;

	/**
	 * Create a new animating sheet.
	 */
	public AnimatingSheet() {
		super();
		setOpaque(true);
	}

	/**
	 * @param pSource new source of this animating sheet
	 */
	public void setSource(final JComponent pSource) {
		this.source = pSource;
		animatingSize.width = pSource.getWidth();
		try {
			makeOffscreenImage();
		} catch (NullPointerException e) {
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			setSource(pSource);
		}
	}

	/**
	 * @param pHeight the new animating height
	 */
	public void setAnimatingHeight(final int pHeight) {
		animatingSize.height = pHeight;
		setSize(animatingSize);
	}

	/**
	 * Create an offscreen image from our source component.
	 */
	private void makeOffscreenImage() {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsConfiguration gfxConfig = ge.getDefaultScreenDevice().getDefaultConfiguration();
		
		offscreenImage = gfxConfig.createCompatibleImage(source.getWidth(), source.getHeight());
		Graphics2D offscreenGraphics = (Graphics2D) offscreenImage.getGraphics();
		
		// windows workaround
		offscreenGraphics.setColor(source.getBackground());
		offscreenGraphics.fillRect(0, 0, source.getWidth(), source.getHeight());
		
		// paint from source to offscreen buffer
		source.paint(offscreenGraphics);
	}

	@Override
	public Dimension getPreferredSize() {
		return animatingSize;
	}

	@Override
	public Dimension getMinimumSize() {
		return animatingSize;
	}

	@Override
	public Dimension getMaximumSize() {
		return animatingSize;
	}

	@Override
	public void update(final Graphics g) {
		// override to eliminate flicker from
		// unnecessary clear
		paint(g);
	}

	@Override
	public void paint(final Graphics g) {
		// get the top-most n pixels of source and
		// paint them into g, where n is height
		// (different from sheet example, which used bottom-most)
		BufferedImage fragment = offscreenImage.getSubimage(0, 0, source
				.getWidth(), animatingSize.height);
		g.drawImage(fragment, 0, 0, this);
	}
	
}