package org.bitfountain.gui.notifications;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.Timer;

/**
 * Slide-in notification tips.
 * 
 * @author Joshua Marinacci
 * @author Chris Adamson 
 */
public class Notification extends Object {

	/** Animation time. */
	protected static final int ANIMATION_TIME = 500;
	
	/** Animation delay. */
	protected static final int ANIMATION_DELAY = 50;

	/** Windows used to display the nofication. */
	JWindow window;
	
	/** Contents of our notification. */
	JComponent contents;
	
	/** Animating sheet used to really display the notifiaction. */
	AnimatingSheet animatingSheet;
	
	/** Dimensions of the desktop. */
	Rectangle desktopBounds;
	
	/** Size of the temporary window. */
	Dimension tempWindowSize;
	
	/** Timer used for the animation. */
	Timer animationTimer;
	
	/** x coordinate where the notification is displayed. */
	int showX;
	
	/** start y coordinate of our notification. */
	int startY;
	
	/** Start time of the animation. */
	long animationStart;

	/**
	 * Create a new empty notification.
	 */
	public Notification() {
		initDesktopBounds();
	}

	/**
	 * Create a new notification with a given content.
	 * @param pContents The content of the notification.
	 */
	public Notification(final JComponent pContents) {
		this();
		setContents(pContents);
	}

	/**
	 * Initialize the desktop bounds.
	 */
	protected void initDesktopBounds() {
		GraphicsEnvironment env = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		desktopBounds = env.getMaximumWindowBounds();
	}

	/**
	 * @param pContents new contents of this notification
	 */
	public void setContents(final JComponent pContents) {
		this.contents = pContents;
		JWindow tempWindow = new JWindow();
		tempWindow.getContentPane().add(pContents);
		tempWindow.pack();
		tempWindowSize = tempWindow.getSize();
		tempWindow.getContentPane().removeAll();
		window = new JWindow();
		animatingSheet = new AnimatingSheet();
		animatingSheet.setSource(pContents);
		window.getContentPane().add(animatingSheet);
	}

	/**
	 * Show a slide-in notification at the specified x coordinate.
	 * @param x x coordinate to show the notification to
	 */
	public void showAt(final int x) {
		// create a window with an animating sheet
		// copy over its contents from the temp window
		// animate it
		// when done, remove animating sheet and add real contents

		showX = x;
		startY = desktopBounds.y + desktopBounds.height;

		ActionListener animationLogic = new ActionListener() {

			public void actionPerformed(final ActionEvent e) {

				long elapsed = System.currentTimeMillis() - animationStart;
				if (elapsed > ANIMATION_TIME) {
					// put real contents in window and show
					window.getContentPane().removeAll();
					window.getContentPane().add(contents);
					window.pack();
					window.setLocation(showX, startY - window.getSize().height);
					window.setVisible(true);
					window.repaint();
					animationTimer.stop();
					animationTimer = null;
				} else {
					// calculate % done
					float progress = (float) elapsed / (float) ANIMATION_TIME;
					// get height to show
					int animatingHeight = (int) (progress * tempWindowSize.getHeight());
					animatingHeight = Math.max(animatingHeight, 1);
					animatingSheet.setAnimatingHeight(animatingHeight);
					window.pack();
					window.setLocation(showX, startY - window.getHeight());
					window.setVisible(true);
					window.repaint();
				}
			}
		};
		animationTimer = new Timer(ANIMATION_DELAY, animationLogic);
		animationStart = System.currentTimeMillis();
		animationTimer.start();
	}
	
	/**
	 * Hide the notification.
	 */
	public void hide() {
		window.setVisible(false);
	}
	
	/**
	 * Show a notification for 2ms.
	 * 
	 * @param message the message to show.
	 */
	public static void show(final String message) {
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(500, 100));
		panel.setLayout(new BorderLayout());
		panel.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		
		JLabel title = new JLabel("Bitfountain");
		title.setOpaque(true);
		title.setBackground(Color.decode("#FFFFFF"));
		title.setFont(Font.decode("Georgia").deriveFont(20.0f));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.lightGray),
				BorderFactory.createEmptyBorder(3, 3, 3, 3)
		));
		panel.add(title, BorderLayout.NORTH);
		
		JLabel text = new JLabel(message);
		text.setHorizontalAlignment(SwingConstants.CENTER);
		text.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(text, BorderLayout.CENTER);
		
		final Notification notif = new Notification(panel);
		notif.showAt(Toolkit.getDefaultToolkit().getScreenSize().width - 500);
		new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(2000L + ANIMATION_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notif.hide();
			}
		} .start();
		
	}

}