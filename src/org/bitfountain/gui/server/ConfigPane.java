package org.bitfountain.gui.server;

import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Configuration pane for the server.
 * 
 * @author Amos Wenger
 */
public class ConfigPane extends JPanel implements ChangeListener {

	/** Generated UID for serialization. */
	private static final long serialVersionUID = 5755371679974984038L;

	/** The server application we're configuring. */
	ServerApp serverApp;
	
	/** Slider to adjust loss percentage. */
	JSlider lossSlider;
	
	/**
	 * Create a new ConfigPane for a ServerApp.
	 * 
	 * @param pServerApp The server application we're configuring
	 */
	@SuppressWarnings("boxing")
	public ConfigPane(final ServerApp pServerApp) {
		
		this.serverApp = pServerApp;
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		
		lossSlider = new JSlider(0, 10, 0);
		
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		for (int i = 0; i <= lossSlider.getMaximum(); i++) {
			labelTable.put(i, new JLabel(i + "%"));			
		}
		lossSlider.setLabelTable(labelTable);
		lossSlider.setMajorTickSpacing(1);
		lossSlider.setPaintTicks(true);
		
		lossSlider.addChangeListener(this);
		lossSlider.setPaintLabels(true);
		JLabel label = new JLabel("Percentage of packets lost:");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		add(label);
		add(lossSlider);
		
	}

	@Override
	public final void stateChanged(final ChangeEvent e) {
		
		if (e.getSource() == lossSlider) {
			serverApp.getServerComm().setLoss(lossSlider.getValue());
		}
		
	}
	
}
