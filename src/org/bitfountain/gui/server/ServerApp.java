package org.bitfountain.gui.server;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import org.bitfountain.io.Logger;
import org.bitfountain.network.server.ServerCommunication;

/**
 * Server-side application, to monitor file sending throughout the network. 
 * 
 * @author Amos Wenger
 * @author Rivo Vasta
 */
public class ServerApp implements Logger {
	
	/** Where log messages are displayed. */
	JTextArea console;
	
	/** Tabs. */
	JTabbedPane tabs;
	
	/** Server-side communication code. */
	ServerCommunication serverComm;
	
	/**
	 * Create a new ServerApp.
	 * @throws IOException Because half of the JDK's methods potentially throw it.
	 */
	public ServerApp() throws IOException {
		// Use GTK look and feel
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		} catch (Exception e) { e.printStackTrace(); }
		
		JFrame frame = new JFrame("Bitfountain - Server");
		frame.setSize(1000, 600);
		frame.setLocation(40, 40);
		
		frame.setLayout(new BorderLayout());
		createMenu(frame);
		
		tabs = new JTabbedPane();
		frame.add(BorderLayout.CENTER, tabs);
		
		console = new JTextArea();
		console.setEditable(false);
		console.setFont(Font.decode("Courier").deriveFont(13.0f));
		tabs.add("Console", new JScrollPane(console));
		
		tabs.add("Settings", new JScrollPane(new ConfigPane(this)));
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		serverComm = new ServerCommunication(this);
		frame.setVisible(true);
	}
	
	/**
	 * @return the server communication used
	 */
	public final ServerCommunication getServerComm() {
		return serverComm;
	}

	/**
	 * Create the menus.
	 * @param frame Frame to add the menu to
	 */
	private void createMenu(final JFrame frame) {
		
		JMenuBar menuBar = new JMenuBar();
		frame.add(BorderLayout.NORTH, menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem fileQuit = new JMenuItem("Quit");
		fileQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(fileQuit);
		
	}

	@Override
	public final void log(final String message) {
		console.append(message + "\n");
	}
	
	/**
	 * Application entry point.
	 * 
	 * @param argv command-line arguments.
	 * @throws IOException In case there's an I/O error.
	 */
	public static void main(final String[] argv) throws IOException {
		new ServerApp();
	}
	
}
 