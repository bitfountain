package org.bitfountain.gui.client;

import javax.swing.UIManager;

import org.bitfountain.network.client.ClientCommunication;


/**
 * Client-side application, to send and receive files.
 * 
 * @author Amos Wenger
 * @author Rivo Vasta
 */
public class ClientApp {

	/** Each window of the ClientApp is associated to one client. */
	private ClientCommunication client;
	
	/** Our main application window. */
	private MainWindow mainWindow;
	
	/** User preferences. */
	private Preferences prefs = new Preferences();
	
	/**
	 * Client application entry point.
	 * 
	 * @param argv Command-line arguments
	 * @throws Exception In case anything goes wrong (duh.)
	 */
	public static void main(final String[] argv) throws Exception {
		// The following instruction allow the use of the GTK look and feel.
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		} catch (Exception e) { e.printStackTrace(); }
		
		new ClientApp();
	}

	/**
	 * Create a new client application.
	 * 
	 * @throws Exception if any IO problem occurs.
	 */
	public ClientApp() throws Exception {
		new LoginWindow(this).login();
	}
	
	/**
	 * Called by LoginWindow when the dialog is completed or aborted.
	 * @param pClient The client
	 * 
	 * @throws Exception If anything goes wrong
	 */
	public void onLogin(final ClientCommunication pClient) throws Exception {
		this.client = pClient;
		
		if (client == null) {
			prefs.save();
			System.exit(0);
		}
		
		this.mainWindow = new MainWindow(this);
		
		while (true) {
			client.step();
			mainWindow.step();
			
			if (client.isIdle()) {
				Thread.sleep(200L);
			}
		}
	}
	
	/**
	 * @return the client associated to this ClientApp.
	 */
	public final ClientCommunication getClient() {
		return client;
	}
	
	/**
	 * @return the main window of our interface
	 */
	public final MainWindow getMainWindow() {
		return mainWindow;
	}
	
	/**
	 * @return the user preferences.
	 */
	public Preferences getPrefs() {
		return prefs;
	}
	
}
