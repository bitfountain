package org.bitfountain.gui.client;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Menu bar with items such as 'Open File', etc.
 * 
 * @author Rivo Vasta
 */
public class MenuBar extends JMenuBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5009776529804800607L;
	
	/** To handle events. */
	private EventHandler eventHandler;
	
	/** The file menu. */
	private JMenu fileMenu;
	
	/** The help menu. */
	private JMenu helpMenu;
	
	/**
	 * Create a new menu bar.
	 * 
	 * @param pEventHandler The event handler.
	 */
	public MenuBar(final EventHandler pEventHandler) {
		this.eventHandler = pEventHandler;
		initFileMenu();
		initHelpMenu();
	}
	
	/**
	 * Create the file menu.
	 */
	private void initFileMenu() {
		this.fileMenu = new JMenu("Fichier");
				
		JMenuItem quit = new JMenuItem("Quitter");
		quit.setActionCommand("quit");
		quit.addActionListener(this.eventHandler);
		fileMenu.add(quit);
		
		this.add(fileMenu);
	}

	/**
	 * Create the help menu.
	 */
	private void initHelpMenu() {
		this.helpMenu = new JMenu("?");
		JMenuItem about = new JMenuItem("À propos");
		about.setActionCommand("about");
		about.addActionListener(this.eventHandler);
		helpMenu.add(about);
		
		this.add(helpMenu);
	}
	
}

