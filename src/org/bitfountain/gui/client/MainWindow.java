package org.bitfountain.gui.client;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.bitfountain.gui.client.catalog.CatalogList;
import org.bitfountain.gui.client.download.DownloadList;
import org.bitfountain.gui.client.upload.UploadButtons;
import org.bitfountain.gui.client.upload.UploadList;

/**
 * This is the main class of the GUI, the class of the main window.
 *
 * @author Rivo Vasta
 */

public class MainWindow extends JFrame {
	
	/** Auto-generated UID for serialization. */
	private static final long serialVersionUID = -1727961789011262157L;
	
	/** Reference to the ClientApp using this MainWindow. */
	private final ClientApp clientApp;
	
	/** Main event handler. */
	EventHandler eventHandler;
	
	/** Menu bar with 'Open File' items, etc. */
	MenuBar menuBar;
	
	/** The panel for tabs. */
	JTabbedPane tabbedPane;
	
	
	/** The transferring files tab. */
	JPanel downloadingFiles;
		
	/** List of files that are being downloaded. */
	DownloadList downloadList;
	
	
	/** The available files tab. */
	JPanel availableFiles;
	
	/** List of files that are available. */
	CatalogList catalogList;
	
	/** The uploading files tab. */
	JPanel uploadingFiles;
	
	/** "Uploading files" buttons. */
	UploadButtons uploadButtons;
	
	/** List of files that are being uploaded. */
	UploadList uploadList;
	
	
	/**
	 * Create a new main window.
	 * @param pClientApp the ClientApp associated with this MainWindow
	 */
	public MainWindow(final ClientApp pClientApp) {
		this.clientApp = pClientApp;
		this.eventHandler = new EventHandler(this);
		String title = " Bitfountain of " + clientApp.getClient().getName() + " ";
		this.setTitle(title);
		this.setSize(700, 500);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
                eventHandler.quit();
            }
		});
		
		// Menubar instructions.
		this.menuBar = new MenuBar(this.eventHandler);
		this.setJMenuBar(this.menuBar);

		// Tabbed pane instructions.
		this.tabbedPane = new JTabbedPane();
		this.downloadingFiles = new JPanel();
		this.availableFiles = new JPanel();
		this.uploadingFiles = new JPanel();
		
		// "Transferring files"'s tab instructions.
		this.downloadList = new DownloadList(this);
		downloadingFiles.setLayout(new BorderLayout());
		downloadingFiles.add(this.downloadList, BorderLayout.CENTER);
		this.tabbedPane.addTab("Téléchargements", downloadingFiles);
		
		// "Available files"'s tab instructions.
		this.catalogList = new CatalogList(this);
		availableFiles.setLayout(new BorderLayout());
		availableFiles.add(this.catalogList, BorderLayout.CENTER);
		this.tabbedPane.addTab("Catalogue", availableFiles);
		
		// "Uploading files"'s tab instructions.
		this.uploadButtons = new UploadButtons(this);
		this.uploadList = new UploadList(this);
		uploadingFiles.setLayout(new BorderLayout());
		uploadingFiles.add(this.uploadButtons, BorderLayout.PAGE_START);
		uploadingFiles.add(this.uploadList, BorderLayout.CENTER);
		this.tabbedPane.addTab("Téléversements", uploadingFiles);
		
		
		// Final instructions for integration in the main window and setting it visible.
		this.add(this.tabbedPane, BorderLayout.CENTER);
		this.setVisible(true);
		
	}
	
	/**
	 * @return the ClientApp
	 */
	public final ClientApp getClientApp() {
		return clientApp;
	}
	
	/**
	 * @return our event handler
	 */
	public final EventHandler getEventHandler() {
		return eventHandler;
	}
	
	/**
	 * Method that will be called in an infinite loop to
	 * update each progress bar of each element of each
	 * filezones (up- and downloading zone).
	 */
	public final void step() {
		this.downloadList.step();
		this.uploadList.step();
	}
	
}