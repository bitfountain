package org.bitfountain.gui.client.upload;


import org.bitfountain.gui.client.MainWindow;
import org.bitfountain.gui.client.transfer.TransferList;
import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;
import org.bitfountain.network.packets.FileHeader;

/**
 * A filezone for uploading files.
 * 
 * @author Rivo Vasta
 */
public class UploadList extends TransferList {

	/** UID for serialization. */
	private static final long serialVersionUID = 4363112161933445903L;

	/**
	 * Create a new filezone for uploading files.
	 * 
	 * @param pMainWindow the main window which this zone is attached to.
	 */
	public UploadList(final MainWindow pMainWindow) {
		super(pMainWindow);
	}
	
	@Override
	public final UploadItem getSelectedElement() {
		return (UploadItem) super.getSelectedElement();
	}
	
	/**
	 * Add a file to the list.
	 * 
	 * @param file The file to add to the list
	 * @param encoder the encoder associated to this file
	 */
	public final void addFile(final FileHeader file, final RSNetEncoder encoder) {
		model.addElement(new UploadItem(file, encoder, mainWindow.getEventHandler()));
	}

	/**
	 * Clean up the uploads list, removing finished uploads.
	 */
	public void clear() {
		for (int i = 0; i < model.getSize(); i++) {
			UploadItem item = (UploadItem) model.getElementAt(i);
			if (item.getProgressive().isFinished()) {
				model.removeElementAt(i);
				i--;
			}
		}
	}
}

