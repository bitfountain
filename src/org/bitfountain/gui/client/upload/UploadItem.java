package org.bitfountain.gui.client.upload;

import javax.swing.JButton;

import org.bitfountain.gui.ImageUtils;
import org.bitfountain.gui.client.EventHandler;
import org.bitfountain.gui.client.transfer.TransferItem;
import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;
import org.bitfountain.network.packets.FileHeader;

/**
 * An element of the Uploading zone.
 * 
 * @author Rivo Vasta
 */
public class UploadItem extends TransferItem {
	
	/** UID for serialization. */
	private static final long serialVersionUID = 5399084414208602582L;

	/**
	 * The encoder used to encode the file before sending it to the server.
	 * Used to update the progressbar.
	 */
	RSNetEncoder encoder;
	
	/** Button that asks for a refresh of the available files list. */
	protected JButton cancelButton;
	
	/**
	 * Create an element of the Uploading zone.
	 * 
	 * @param pFile The file associated to this Uploading zone's element
	 * @param pEncoder The encoder used to encode the file before sending it to the server
	 * @param eventHandler Our event handler
	 */
	public UploadItem(final FileHeader pFile, final RSNetEncoder pEncoder, final EventHandler eventHandler) {
		super(pFile);
		this.encoder = pEncoder;
		
		cancelButton = new JButton(ImageUtils.getTinyIcon("icons/actions/cancel.png"));
		cancelButton.setActionCommand("cancelUpload");
		cancelButton.addActionListener(eventHandler);
		cancelButton.setBorderPainted(false);
		super.buttons.add(cancelButton);
	}
	
	/**
	 * An estimation of the progress of the file emission, from 0.0 to 1.0.
	 * @return an estimation of the progress of the file emission, from 0.0 to 1.0
	 */
	@Override
	public final RSNetEncoder getProgressive() {
		return encoder;
	}
}