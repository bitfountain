package org.bitfountain.gui.client.upload;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.bitfountain.gui.ImageUtils;
import org.bitfountain.gui.client.MainWindow;

/**
 * Toolbar of the "Uploading files" tab.
 * 
 * @author Rivo Vasta
 */
public class UploadButtons extends JPanel {

	/** UID for serialiazing. */
	private static final long serialVersionUID = -5005248421883648901L;

	/** Button that asks the user which files he wants to upload. */
	private JButton uploadButton;
	
	/** Button that cleans finished uploads. */
	private JButton clearButton;
	
	/** Reference to the main window, to pass back events. */
	protected final MainWindow mainWindow;
	
	/**
	 * Creates a new toolbar for the "Available files" tab.
	 * 
	 * @param pMainWindow the main window
	 */
	public UploadButtons(final MainWindow pMainWindow) {
		this.mainWindow = pMainWindow;
		
		((FlowLayout) (this.getLayout())).setAlignment(FlowLayout.LEFT);
		
		uploadButton = new JButton("Téléverser", ImageUtils.getTinyIcon("icons/actions/upload.png"));
		uploadButton.setActionCommand("upload");
		uploadButton.addActionListener(mainWindow.getEventHandler());
		add(uploadButton);
		
		clearButton = new JButton("Nettoyer", ImageUtils.getTinyIcon("icons/actions/clear.png"));
		clearButton.setActionCommand("clear");
		clearButton.addActionListener(mainWindow.getEventHandler());
		add(clearButton);
	}
	
}

