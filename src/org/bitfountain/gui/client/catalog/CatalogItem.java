package org.bitfountain.gui.client.catalog;

import java.awt.Component;

import javax.swing.JButton;

import org.bitfountain.gui.ImageUtils;
import org.bitfountain.gui.client.items.Item;
import org.bitfountain.network.packets.FileHeader;

/**
 * Item of a filezone, especially designed
 * for the CatalogList.
 * 
 * @author Rivo Vasta
 */
public class CatalogItem extends Item {
	
	/** Actual list of items we can download. */
	private final CatalogList catalogList;

	/** UID for serialization. */
	private static final long serialVersionUID = -3961025335072001514L;
	
	/**
	 * A FileHeader containing some very useful informations about
	 * the file of this CatalogItem.
	 */
	FileHeader fileHeader;
	
	/** Button that triggers the download. */
	private JButton downloadButton;
	
	/**
	 * Creates a new element for the CatalogList
	 * from a given file.
	 * 
	 * @param pFile File from which to create an element.
	 * @param pCatalogList Zone of available elements.
	 */
	public CatalogItem(final CatalogList pCatalogList, final FileHeader pFile) {
		super(pFile.getFileName(), pFile.getFileSize());
		this.catalogList = pCatalogList;
		this.fileHeader = pFile;
		
		downloadButton = new JButton(ImageUtils.getTinyIcon("icons/actions/download.png"));
		downloadButton.setActionCommand("download");
		downloadButton.addActionListener(this.catalogList.getMainWindow().getEventHandler());
		downloadButton.setBorderPainted(false);
		super.buttons.add(downloadButton);
	}
	
	/**
	 * @return the file header info which is available.
	 */
	public final FileHeader getFileHeader() {
		return fileHeader;
	}
	
	@Override
	public final void onClick(final int x, final int y, final int button) {
		Component component = buttons.getComponentAt(
				x - buttons.getLocation().x,
				y - buttons.getLocation().y);
		if (component instanceof JButton) {
			((JButton) component).doClick();
		}
	}
	
}