package org.bitfountain.gui.client.catalog;

import java.util.Enumeration;

import org.bitfountain.gui.client.MainWindow;
import org.bitfountain.gui.client.items.Item;
import org.bitfountain.gui.client.items.ItemList;
import org.bitfountain.gui.notifications.Notification;
import org.bitfountain.network.packets.FileHeader;

/**
 * List of files available.
 * 
 * @author Rivo Vasta
 */
public class CatalogList extends ItemList {

	/** UID for serialization. */
	private static final long serialVersionUID = -6198834337476571808L;
	
	/**
	 * Create a new file zone for a given main window.
	 *
	 * @param pMainWindow The main window to create a file zone for.
	 */
	public CatalogList(final MainWindow pMainWindow) {
		super(pMainWindow);
	}
	
	@Override
	public final CatalogItem getSelectedElement() {
		return (CatalogItem) super.getSelectedElement();
	}
	
	/**
	 * Add a file to the list.
	 * 
	 * @param fileHeader The file to add to the list
	 */
	public final void addFile(final FileHeader fileHeader) {
		for (Enumeration<?> e = model.elements(); e.hasMoreElements();) {
			Item item = (Item) e.nextElement();
			long fileId = new FileHeader(item.getFileName(), item.getFileSize()).getFileId();
			if (fileId == fileHeader.getFileId()) {
				return;
			}
		}
		Notification.show("Nouveau fichier disponible " + fileHeader.getFileName());
		model.addElement(new CatalogItem(this, fileHeader));
	}
	
}

