package org.bitfountain.gui.client;

/**
 * Interface to be implemented by every class which is interested
 * into the progress of an operation, quantifiable in bytes. 
 * 
 * @author Amos Wenger
 */
public interface ProgressListener {

	/**
	 * Is called whenever there's progress in a process (such as encoding or decoding).
	 * 
	 * @param bytesDone Bytes already processed
	 * @param bytesTotal Total bytes to process
	 * @param percentage The progress of the task, in percents.
	 * @param secondsRemaining Estimated Time of Arrival, in seconds
	 */
	void onProgress(long bytesDone, long bytesTotal, int percentage, int secondsRemaining);
	
}
