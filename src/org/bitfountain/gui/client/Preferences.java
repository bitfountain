package org.bitfountain.gui.client;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.bitfountain.gui.notifications.Notification;
import org.bitfountain.network.server.ServerCommunication;

/**
 * Handle client-side persistent user preferences.
 * 
 * @author Amos Wenger
 */
public class Preferences {

	/** Usual path to store preferences. */
	private final String preferencesPath = System.getProperty("user.home")
		+ File.separator + ".config" + File.separator + "bitfountain"
		+ File.separator + "bitfountain.cfg";
	
	/** Properties object used to load/store preferences. */
	Properties props = new Properties();
	
	/**
	 * Create a preferences object, loading them from the default path.
	 */
	public Preferences() {
		try {
			props.load(new FileReader(preferencesPath));
		} catch (Exception e) {
			Notification.show("Welcome to Bitfountain!");
		}
	}
	
	/**
	 * @return the last upload directoy used.
	 */
	public String getLastUploadDirectory() {
		return props.getProperty("lastUploadDir", System.getProperty("user.home"));
	}
	
	/**
	 * @return the last upload directoy used.
	 */
	public String getLastServer() {
		return props.getProperty("lastServer", "localhost");
	}
	
	/**
	 * @return the last port used.
	 */
	public int getLastPort() {
		return Integer.parseInt(props.getProperty("lastPort", String.valueOf(ServerCommunication.DEFAULT_PORT)));
	}
	
	/**
	 * @return the last username used.
	 */
	public String getLastUsername() {
		return props.getProperty("lastUserName", System.getProperty("user.name"));
	}
	
	/**
	 * @param lastUploadDir The last used upload directory.
	 */
	public void setLastUploadDirectory(final String lastUploadDir) {
		props.setProperty("lastUploadDir", lastUploadDir);
	}
	
	/**
	 * @param lastServer The last used upload directory.
	 */
	public void setLastServer(final String lastServer) {
		props.setProperty("lastServer", lastServer);
	}
	
	/**
	 * @param lastPort The last used port
	 */
	public void setLastPort(final int lastPort) {
		props.setProperty("lastPort", String.valueOf(lastPort));
	}
	
	/**
	 * @param lastUserName the last username used
	 */
	public void setLastUsername(final String lastUserName) {
		props.setProperty("lastUserName", lastUserName);
	}

	/**
	 * save preferences. 
	 */
	public void save() {
		try {
			new File(preferencesPath).getParentFile().mkdirs();
			props.store(new FileWriter(preferencesPath), "");
		} catch (IOException e) {
			// well, too bad.
			return;
		}
	}
	
}
