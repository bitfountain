package org.bitfountain.gui.client.download;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bitfountain.gui.client.MainWindow;
import org.bitfountain.gui.client.transfer.TransferList;
import org.bitfountain.math.reedsolomon.decoding.RSNetDecoder;
import org.bitfountain.network.packets.FileHeader;

/**
 * A filezone for downloading files.
 * 
 * @author Rivo Vasta
 */
public class DownloadList extends TransferList {

	/** UID for serialization. */
	private static final long serialVersionUID = 4363112161933445903L;

	/**
	 * Create a new filezone for downloading files.
	 * 
	 * @param pMainWindow the main window which this zone is attached to.
	 */
	public DownloadList(final MainWindow pMainWindow) {
		super(pMainWindow);
		super.getFileList().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent e) {
//				pMainWindow.downloadingButtons.onSelect(getSelectedElement());
			}
		});
	}
	
	@Override
	public final DownloadItem getSelectedElement() {
		return (DownloadItem) super.getSelectedElement();
	}
	
	/**
	 * Add a file to the list.
	 * 
	 * @param file The file to add to the list
	 * @param decoder the decoder associated to this file
	 */
	public final void addFile(final FileHeader file, final RSNetDecoder decoder) {
		model.addElement(new DownloadItem(file, decoder, mainWindow.getEventHandler()));
	}
}

