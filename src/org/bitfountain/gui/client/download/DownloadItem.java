package org.bitfountain.gui.client.download;

import javax.swing.JButton;

import org.bitfountain.gui.ImageUtils;
import org.bitfountain.gui.client.EventHandler;
import org.bitfountain.gui.client.transfer.TransferItem;
import org.bitfountain.math.reedsolomon.decoding.RSNetDecoder;
import org.bitfountain.network.packets.FileHeader;

/**
 * An element of the Downloading zone.
 * 
 * @author Rivo Vasta
 */
public class DownloadItem extends TransferItem {
	
	/** UID for serialization. */
	private static final long serialVersionUID = 5399084414208602582L;

	/**
	 * The decoder used to decode the file when it is completely downloaded.
	 * Used to update the progressbar.
	 */
	RSNetDecoder decoder;
	
	/** Button that asks for a refresh of the available files list. */
	protected JButton cancelButton;
	
	/** Button that triggers decoding of a file. */
	private JButton openButton;
	
	/** Button to delete a file of the filezone when undesired anymore. */
	private JButton deleteButton;
	
	/**
	 * Create an element of the Downloading zone.
	 * 
	 * @param pFile The file associated to this Downloading zone's element
	 * @param pDecoder The decoder used to decode the file when it is completely downloaded
	 * @param eventHandler Our event handler.
	 */
	public DownloadItem(final FileHeader pFile, final RSNetDecoder pDecoder, final EventHandler eventHandler) {
		super(pFile);
		this.decoder = pDecoder;
		
		cancelButton = new JButton(ImageUtils.getTinyIcon("icons/actions/cancel.png"));
		cancelButton.setActionCommand("cancelDownload");
		cancelButton.addActionListener(eventHandler);
		cancelButton.setBorderPainted(false);
		super.buttons.add(cancelButton);
		
		openButton = new JButton(ImageUtils.getTinyIcon("icons/actions/open.png"));
		openButton.setActionCommand("open");
		openButton.addActionListener(eventHandler);
		openButton.setBorderPainted(false);
		super.buttons.add(openButton);
		
		deleteButton = new JButton(ImageUtils.getTinyIcon("icons/actions/delete.png"));
		deleteButton.setActionCommand("delete");
		deleteButton.addActionListener(eventHandler);
		deleteButton.setBorderPainted(false);
		super.buttons.add(deleteButton);
		
	}
	
	/**
	 * An estimation of the progress of the file reception, from 0.0 to 1.0.
	 * 
	 * @return an estimation of the progress of the file reception, from 0.0 to 1.0
	 */
	@Override
	public final RSNetDecoder getProgressive() {
		return decoder;
	}
	
}