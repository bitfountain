package org.bitfountain.gui.client;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.bitfountain.gui.client.catalog.CatalogItem;
import org.bitfountain.gui.client.download.DownloadItem;
import org.bitfountain.gui.client.upload.UploadItem;
import org.bitfountain.gui.notifications.Notification;
import org.bitfountain.math.reedsolomon.decoding.RSNetDecoder;
import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FileList;

/**
 * Handle most actions in the client GUI.
 * 
 * @author Rivo Vasta
 */
public class EventHandler implements ActionListener {

	/** Used to FORMAT percentages etc. */
	public static final DecimalFormat FORMAT = new DecimalFormat("0.00");
	
	/** Our main window. */
	MainWindow mainWindow;
	
	/** File chooser used to.. choose which files to upload to the server. */
	JFileChooser jfc;
	
	/**
	 * Create a new event handler for a given class.
	 * 
	 * @param pMainWindow the main window.
	 */
	public EventHandler(final MainWindow pMainWindow) {
		this.mainWindow = pMainWindow;
		
		this.jfc = new JFileChooser(mainWindow.getClientApp().getPrefs().getLastUploadDirectory());
		this.jfc.setMultiSelectionEnabled(true);
	}

	@Override
	public final void actionPerformed(final ActionEvent e) {
		String source = e.getActionCommand();
		
		if (source.equals("quit")) {
			quit();
		} else if (source.equals("cancelDownload")) {
			cancelDownload();
		} else if (source.equals("cancelUpload")) {
			cancelUpload();
		} else if (source.equals("clear")) {
			clear();
		} else if (source.equals("open")) {
			open();
		} else if (source.equals("download")) {
			download();
		} else if (source.equals("upload")) {
			upload();
		} else if (source.equals("delete")) {
			delete();
		} else if (source.equals("about")) {
			about();
		} else {
			throw new Error("Bad button command sent to the event handler… should never arrive, akshully.");
		}
	}

	/**
	 * Quit.
	 */
	public final void quit() {
		mainWindow.getClientApp().getPrefs().save();
		System.exit(0);
	}
		
	/**
	 * Cancel a current download.
	 */
	private void cancelDownload() {
		final DownloadItem element = mainWindow.downloadList.getSelectedElement();
		
		FileHeader header = element.getProgressive().getHeader();
		mainWindow.getClientApp().getClient().cancelDownload(header);
		mainWindow.downloadList.removeFile(header);
		
		Notification.show("Téléchargement de " + element.getFileName() + " annulé.");		
	}
	
	/**
	 * Cancel a current upload.
	 */
	private void cancelUpload() {
		final UploadItem element = mainWindow.uploadList.getSelectedElement();
		
		FileHeader header = element.getProgressive().getHeader();
		mainWindow.getClientApp().getClient().cancelUpload(header);
		mainWindow.uploadList.removeFile(header);
		
		Notification.show("Téléversement de " + element.getFileName() + " annulé.");		
	}
	
	/**
	 * Clears the list of finished downloads/uploads.
	 */
	private void clear() {
		mainWindow.uploadList.clear();
		
	}
	
	/**
	 * Open the selected file.
	 */
	private void open() {
		final DownloadItem element = mainWindow.downloadList.getSelectedElement();
		if (element == null) { return; }
		
		try {
			Desktop.getDesktop().open((element.getProgressive().getDestination()));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"Couldn't open file.",
					"Error while trying to open file", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	/**
	 * Just received a file list from the server, refreshing.
	 * @param fileList The file list we just received
	 */
	public final void gotFileList(final FileList fileList) {
		for (FileHeader file : fileList) {
			mainWindow.catalogList.addFile(file);
		}
	}
	
	/**
	 * Send a file request to the server.
	 */
	private void download() {
		final CatalogItem element = mainWindow.catalogList.getSelectedElement();
		if (element == null) { return; }
		
		try {
			// Ask the server to send the file.
			RSNetDecoder decoder = mainWindow.getClientApp().getClient().receiveFile(element.getFileHeader(),
					new File(System.getProperty("user.home") + File.separator
							+ "Downloads" + File.separator + element.getFileHeader().getFileName()));
			
			// Add the element to the TransferList.
			mainWindow.downloadList.addFile(element.getFileHeader(), decoder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Remove the element from the CatalogList.
		mainWindow.catalogList.removeFile(element.getFileHeader());

	}
	
	/**
	 * Open a file chooser to allow the user to send a file
	 * to the server, then send it.
	 */
	private void upload() {
		int result = jfc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			mainWindow.getClientApp().getPrefs().setLastUploadDirectory(jfc.getSelectedFile().getParent());
			for (File file : jfc.getSelectedFiles()) {
				RSNetEncoder encoder = mainWindow.getClientApp().getClient().sendFile(file);
				mainWindow.uploadList.addFile(new FileHeader(file), encoder);
			}
		}
	}
	
	/**
	 * Remove a fully downloaded or a cancelled download from the downloaded zone.
	 */
	private void delete() {
		final DownloadItem element = mainWindow.downloadList.getSelectedElement();
		if (element == null) { return; }
		
		mainWindow.downloadList.removeFile(element.getProgressive().getHeader());
	}

	/**
	 * Display an About window.
	 */
	private void about() {
		JOptionPane.showMessageDialog(null,
				"Bitfountain: communist file sharing over wifi\n"
				+ "Copyleft Rivo Vasta & Amos Wenger, 2010",
				"À propos de Bitfountain", JOptionPane.PLAIN_MESSAGE);
	}
	
}

