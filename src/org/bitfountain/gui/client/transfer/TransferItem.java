package org.bitfountain.gui.client.transfer;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.bitfountain.gui.client.items.Item;
import org.bitfountain.network.Progressive;
import org.bitfountain.network.packets.FileHeader;

/**
 * Item of a filezone, especially designed
 * for the TransferList.
 * 
 * @author Rivo Vasta
 */
public abstract class TransferItem extends Item {

	/** UID for serialization. */
	private static final long serialVersionUID = -3961025335072001514L;

	/** Progress bar. */
	JProgressBar progressBar;

	/** Show the speed of the current operation. */
	JLabel speed;
	
	/**
	 * Creates a new element for the TransferList
	 * from a given file.
	 * 
	 * @param pFile File from which to create an element.
	 */
	public TransferItem(final FileHeader pFile) {
		super(pFile.getFileName(), pFile.getFileSize());
		
		this.speed = new JLabel(" — 0.0KiB/s");
		super.fileInfos.add(this.speed);
		
		this.progressBar = new JProgressBar(0, 1000);
		this.progressBar.setValue(0);
		this.progressBar.setStringPainted(true);
		JPanel progressPanel = new JPanel();
		progressPanel.setLayout(new GridLayout(1, 1));
		progressPanel.setOpaque(false);
		progressPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
		progressPanel.add(progressBar);
		
		this.panel.add(progressPanel);
	}
	
	/**
	 * @return an estimation of the progress of the file reception, from 0.0 to 1.0
	 */
	public abstract Progressive getProgressive();
	
	@Override
	public final void onClick(final int x, final int y, final int button) {
		Component component = buttons.getComponentAt(
				x - buttons.getLocation().x,
				y - buttons.getLocation().y);
		if (component instanceof JButton) {
			((JButton) component).doClick();
		}
	}

}