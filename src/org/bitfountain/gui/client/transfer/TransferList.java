package org.bitfountain.gui.client.transfer;

import java.util.Enumeration;

import org.bitfountain.gui.client.EventHandler;
import org.bitfountain.gui.client.MainWindow;
import org.bitfountain.gui.client.items.ItemList;

/**
 * Abstract class for a zone of transferring files
 * (either downloading from or uploading to the server).
 * 
 * @author Rivo Vasta
 * @author Amos Wenger
 */
public abstract class TransferList extends ItemList {
	
	/** UID for serialization. */
	private static final long serialVersionUID = 5201571477533493335L;

	/**
	 * Create a new filezone for a given main window.
	 *
	 * @param pMainWindow The main window to create a file zone for.
	 */
	public TransferList(final MainWindow pMainWindow) {
		super(pMainWindow);
	}
	
	@Override
	public TransferItem getSelectedElement() {
		return (TransferItem) super.getSelectedElement();
	}

	/**
	 * Method to update the progress bars of the elements of
	 * the UploadList.
	 */
	public final void step() {
		for (Enumeration<?> e = model.elements(); e.hasMoreElements();) {
			TransferItem element = (TransferItem) e.nextElement();
			if (element.progressBar.getValue() == 1000 && element.getProgressive().isFinished()) {
				// don't update complete elements.
				continue;
			}
			double progress = 1000 * element.getProgressive().getProgress();
			element.progressBar.setValue((int) progress);
			element.speed.setText(" — " + EventHandler.FORMAT.format(element.getProgressive().getSpeed()) + "KiB/s");
			this.repaint();
		}
	}
	
}

