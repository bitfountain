package org.bitfountain.gui.client.items;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import org.bitfountain.gui.client.MainWindow;
import org.bitfountain.network.packets.FileHeader;

/**
 * Zone displaying a list of items.
 * 
 * @author Amos Wenger
 */
public abstract class ItemList extends JScrollPane {

	/** The main window. */
	protected MainWindow mainWindow;
	
	/** Files being transferred. */
	protected JList fileList;
	
	/** Underlying model to store file list. */
	protected DefaultListModel model;
	
	/** UID for serialization. */
	private static final long serialVersionUID = -482483712627064117L;
	
	/**
	 * Create a new item list.
	 * @param pMainWindow The main window we belong to.
	 */
	public ItemList(final MainWindow pMainWindow) {
		
		this.mainWindow = pMainWindow;
		this.model = new DefaultListModel();
		this.fileList = new JList(model);
		this.fileList.setCellRenderer(new ItemListRenderer());
		
		fileList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				int index = fileList.locationToIndex(e.getPoint());
				if (index == -1) { return; }
				
				Item item = (Item) model.getElementAt(index);
				item.onClick(e.getX(), e.getY() - (item.getPanel().getHeight() * index), e.getButton());
			}
		});
		
		setViewportView(fileList);
		
	}
	
	/**
	 * @return the list of files being transferred.
	 */
	public final JList getFileList() {
		return fileList;
	}
	
	/**
	 * @return the main window we belong to.
	 */
	public final MainWindow getMainWindow() {
		return mainWindow;
	}
	
	/**
	 * Remove a file from the list.
	 * 
	 * @param file The file to remove from the list
	 */
	public final void removeFile(final FileHeader file) {
		int index = 0;
		for (Enumeration<?> e = model.elements(); e.hasMoreElements();) {
			Item item = (Item) e.nextElement();
			if (item.getFileName().equals(file.getFileName())) {
				model.remove(index);
				break;
			}
			index++;
		}
	}
	
	/**
	 * @return the selected element in the file list, or -1 if nothing is selected.
	 */
	public Item getSelectedElement() {
		int index = fileList.getSelectedIndex();
		if (index == -1) {
			return null;
		}
		
		return ((Item) model.get(index));
	}
	
}
