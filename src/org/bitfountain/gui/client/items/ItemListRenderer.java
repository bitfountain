package org.bitfountain.gui.client.items;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Custom cell renderer to display infos about files being transferred.
 * 
 * @author Amos Wenger
 */
public class ItemListRenderer implements ListCellRenderer {

	@Override
	public final Component getListCellRendererComponent(final JList list,
			final Object object, final int index,
			final boolean isSelected, final boolean cellHasFocus) {
		
		Item item = ((Item) object);
		
		if (isSelected) {
			item.setBackground(list.getSelectionBackground());
			item.setForeground(list.getSelectionForeground());
		} else {
			item.setBackground(Color.decode("#EEEEEE"));
			item.setForeground(list.getForeground());
		}
		
		return item;
	}
	
}