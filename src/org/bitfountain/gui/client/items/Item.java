package org.bitfountain.gui.client.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bitfountain.gui.ImageUtils;
import org.bitfountain.gui.MimeTypes;

/**
 * Fundamental element of a filezone,
 * an Item contains all necessary components
 * to describe files in the filezone.
 */
public abstract class Item extends JPanel {

	/** UID for serialization. */
	private static final long serialVersionUID = -1069391155160879557L;

	/** The file concerned by this filezone element. */
	protected File file;

	/** Main panel, contains file infos, buttons and progress bar. */
	protected JPanel panel;
	
	/** Secondary panel, contains files infos and buttons. */
	protected JPanel fileInfosAndButtons;
	
	/** Little panel for the buttons. */
	protected JPanel buttons;

	/** Little panel for file name + file size. */
	protected JPanel fileInfos;

	/** File name for the JLabel. */
	protected JLabel fileNameLabel;
	
	/** File name. */
	protected String fileName;

	/** File size for the JLabel. */
	protected JLabel fileSizeLabel;
	
	/** File size. */
	protected long fileSize;

	/**
	 * Create a new element from a file.
	 * 
	 * @param pFile File from which to create an element.
	 */
	public Item(final File pFile) {
		this(pFile.getName(), pFile.length());
		this.file = pFile;
	}
	
	/**
	 * Create a new element from a file header.
	 * 
	 * @param pFileName the name of the file
	 * @param pFileSize the size of the file
	 */
	public Item(final String pFileName, final long pFileSize) {
		this.file = null;
		
		this.panel = new JPanel();
		this.fileInfosAndButtons = new JPanel();
		this.fileInfos = new JPanel();
		this.buttons   = new JPanel();
		this.fileNameLabel = new JLabel(pFileName);
		this.fileSizeLabel = new JLabel(" — " + (pFileSize / 1024) + " KiB");
		
		this.fileName = pFileName;
		this.fileSize = pFileSize;

		this.setLayout(new BorderLayout());
		this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.PAGE_AXIS));
		this.panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createMatteBorder(0, 0, 1, 0, Color.lightGray),
				BorderFactory.createEmptyBorder(2, 3, 2, 3)
		));
		this.panel.setOpaque(false);
		
		((FlowLayout) (this.fileInfos.getLayout())).setAlignment(FlowLayout.LEFT);
		((FlowLayout) (this.fileInfos.getLayout())).setHgap(0);
		this.fileInfos.setOpaque(false);
		
		((FlowLayout) (this.buttons.getLayout())).setAlignment(FlowLayout.RIGHT);
		this.buttons.setOpaque(false);

		this.fileNameLabel.setFont(this.fileNameLabel.getFont().deriveFont(Font.BOLD));
		String contentType = MimeTypes.getContentType(pFileName);
		ImageIcon imageIcon = ImageUtils.getIcon("icons/mimetypes/" + contentType + ".png");
		this.fileNameLabel.setIcon(imageIcon);
		
		this.fileInfos.add(this.fileNameLabel);
		this.fileInfos.add(this.fileSizeLabel);
	
		this.fileInfosAndButtons.setLayout(new BorderLayout());
		this.fileInfosAndButtons.add(this.fileInfos, BorderLayout.CENTER);
		this.fileInfosAndButtons.add(this.buttons, BorderLayout.LINE_END);
		this.fileInfosAndButtons.setOpaque(false);
		this.panel.add(this.fileInfosAndButtons);
		
		this.add(panel, BorderLayout.CENTER);
		
		this.setOpaque(true);
		this.panel.setOpaque(false);
	}
	
	/**
	 * Return the file name.
	 * 
	 * @return the file name.
	 */
	public final String getFileName() {
		return fileName;
	}
	
	/**
	 * Return the file size.
	 * @return the file size.
	 */
	public final long getFileSize() {
		return fileSize;
	}
	
	/**
	 * @return the JPanel containing the interface linked with this element
	 */
	public final JPanel getPanel() {
		return panel;
	}
	
	/**
	 * Handle a click event.
	 * @param x x coordinate of the click
	 * @param y y coordinate of the click
	 * @param button button of the click.
	 */
	public abstract void onClick(final int x, final int y, final int button);
	
}