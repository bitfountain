package org.bitfountain.gui.client;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.bitfountain.gui.notifications.Notification;
import org.bitfountain.network.client.ClientCommunication;

/**
 * Small dialog allowing a user to log-in.
 * 
 * @author Amos Wenger
 */
public class LoginWindow {

	/** Text fields for the login data. */
	JTextField username, host, port;
	
	/** Becomes non-null when login is successful. */
	ClientCommunication client = null;
	
	/** The application we belong to. */
	ClientApp clientApp;
	
	/** Main dialog frame. */
	JFrame frame;
	
	/**
	 * Create a new login window.
	 * @param pClientApp the application we belong to.
	 */
	public LoginWindow(final ClientApp pClientApp) {
		this.clientApp = pClientApp;
	}
	
	/**
	 * Show the login window.
	 */
	public void login() {
		
		frame = new JFrame("Bitfountain - Login");
		frame.setSize(new Dimension(250, 300));
		frame.setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		KeyAdapter connectOnEnter = new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					tryConnect();
				}
			}
		};
		
		host = new JTextField(clientApp.getPrefs().getLastServer());
		host.addKeyListener(connectOnEnter);
		JPanel hostPanel = new JPanel(new GridLayout(0, 1));
		hostPanel.add(new JLabel("Serveur"));
		hostPanel.add(host);
		panel.add(hostPanel);
		
		port = new JTextField(String.valueOf(clientApp.getPrefs().getLastPort()));
		port.addKeyListener(connectOnEnter);
		JPanel portPanel = new JPanel(new GridLayout(0, 1));
		portPanel.add(new JLabel("Port"));
		portPanel.add(port);
		panel.add(portPanel);
		
		panel.add(Box.createRigidArea(new Dimension(20, 20)));
		
		username = new JTextField(clientApp.getPrefs().getLastUsername());
		username.addKeyListener(connectOnEnter);
		JPanel userPanel = new JPanel(new GridLayout(0, 1));
		userPanel.add(new JLabel("Utilisateur"));
		userPanel.add(username);
		panel.add(userPanel);
		
		panel.add(Box.createRigidArea(new Dimension(20, 20)));
		
		JButton loginButton = new JButton("Se Connecter");
		panel.add(loginButton);
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent ev) {
				tryConnect();
			}
		});
		
		frame.add(panel);
		frame.setVisible(true);

		while (frame.isVisible()) {
			try {
				Thread.sleep(200L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			clientApp.onLogin(client);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Try to connect to the server.
	 */
	void tryConnect() {
		new Thread() {
			@Override
			public void run() {
				int portNumber = 0;
				try {
					portNumber = Integer.parseInt(port.getText());
				} catch (NumberFormatException e) {
					portNumber = -1;
				}
				if (portNumber < 1 || portNumber > 65536) {
					JOptionPane.showMessageDialog(null, "Port doit être un nombre entre 1 et 65536.", "Erreur", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ClientCommunication candidate = new ClientCommunication(clientApp, username.getText());
				try {
					candidate.connectToServer(host.getText(), portNumber);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
					return;
				}
				clientApp.getPrefs().setLastServer(host.getText());
				clientApp.getPrefs().setLastPort(portNumber);
				clientApp.getPrefs().setLastUsername(username.getText());
				Notification.show("Connecté.");
				client = candidate;
				frame.setVisible(false);
			}
		} .start();
	}
	
	/**
	 * @return the client connection we've obtained. May be null if the
	 * login process is incomplete.
	 */
	public ClientCommunication getClient() {
		return client;
	}

}
