package org.bitfountain.gui;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Utility class to figure out a file's content type from its extension.
 * 
 * @author Amos Wenger
 */
public class MimeTypes {

	/** Common audio files extensions. */
	static HashSet<String> audio = new HashSet<String>();
	static {
		audio.add("aac");
		audio.add("aif");
		audio.add("iff");
		audio.add("mp3");
		audio.add("mpa");
		audio.add("ra");
		audio.add("wav");
		audio.add("wma");
		// technically, ogg is a panel, not a FORMAT
		// however, it's much more common to find audio
		// in an ogg file, than video.
		audio.add("ogg");
	}
	
	/** Common image files extensions. */
	static HashSet<String> image = new HashSet<String>();
	static {
		image.add("bmp");
		image.add("gif");
		image.add("jpg");
		image.add("jpeg");
		image.add("png");
		image.add("psd");
		image.add("psp");
		image.add("thm");
		image.add("tif");
		image.add("tga");
		image.add("ai");
		image.add("drw");
		image.add("eps");
		image.add("ps");
		image.add("svg");
		image.add("ico");
	}
	
	/** Common video files extensions. */
	static HashSet<String> video = new HashSet<String>();
	static {
		video.add("3g2");
		video.add("3gp");
		video.add("asf");
		video.add("asx");
		video.add("avi");
		video.add("flv");
		video.add("mov");
		video.add("mp4");
		video.add("mpg");
		video.add("rm");
		video.add("swf");
		video.add("vob");
		video.add("wmv");
		video.add("mkv");
		video.add("ogv");
	}
	
	/** Common system files extensions. */
	static HashSet<String> system = new HashSet<String>();
	static {
		system.add("app");
		system.add("bat");
		system.add("cgi");
		system.add("com");
		system.add("exe");
		system.add("pif");
		system.add("vb");
		system.add("ws");
		
		system.add("cab");
		system.add("cpl");
		system.add("cur");
		system.add("dll");
		system.add("dmp");
		system.add("drv");
		system.add("key");
		system.add("lnk");
		system.add("sys");
		
		system.add("cfg");
		system.add("ini");
	}
	
	/** Common archive extensions. */
	static HashSet<String> archive = new HashSet<String>();
	static {
		archive.add("7z");
		archive.add("deb");
		archive.add("rpm");
		archive.add("gz");
		archive.add("pkg");
		archive.add("rar");
		archive.add("sit");
		archive.add("sitx");
		archive.add("gz");
		archive.add("bz2");
		archive.add("xz");
		archive.add("zip");
		archive.add("zipx");
	}
	
	/** Common text files extension. */
	static HashSet<String> text = new HashSet<String>();
	static {
		text.add("ooc");
		text.add("c");
		text.add("cpp");
		text.add("cxx");
		text.add("h");
		text.add("hpp");
		text.add("hxx");
		text.add("java");
		text.add("pl");
		text.add("php");
		text.add("js");
		text.add("dtd");
		text.add("xml");
		text.add("sh");
		
		text.add("doc");
		text.add("docx");
		text.add("log");
		text.add("msg");
		text.add("pages");
		text.add("rtf");
		text.add("txt");
		text.add("wpd");
		text.add("wps");
		text.add("pdf");
	}
	
	/** Categories of files we care about. */
	static HashMap<String, HashSet<String>> categories = new HashMap<String, HashSet<String>>();
	static {
		categories.put("archive", archive);
		categories.put("audio", audio);
		categories.put("image", image);
		categories.put("system", system);
		categories.put("text", text);
		categories.put("video", video);
	}
	
	/**
	 * Entry point.
	 * @param argv Command-line arguments
	 */
	public static void main(final String[] argv) {
		printInfo("README.txt");
		printInfo("Kyo - Chaque Seconde.mp3");
		printInfo("Mes Vacances en Floride.avi");
		printInfo("Ma cousine en string.jpg");
		printInfo("helloworld.ooc");
		printInfo("wormZ.32s.exe");
		printInfo("rock-0.9.0.tar.bz2");
		printInfo("machin.dat");
	}
	
	/**
	 * @param path The path to test.
	 */
	public static void printInfo(final String path) {
		System.out.println(path + " = " + getContentType(path));
	}
		
	/**
	 * @param path The path of the file we should figure the content type of 
	 * @return the content type of the file, ie. one of 'archive', 'audio',
	 * 'image', 'system', 'text', 'video', or 'generic'
	 */
	public static String getContentType(final String path) {
		
		int indexOf = path.lastIndexOf('.');
		if (indexOf == -1) {
			return "generic";
		}
		
		String extension = path.substring(indexOf + 1).toLowerCase();
		
		for (String category : categories.keySet()) {
			if (categories.get(category).contains(extension)) {
				return category;
			}
		}
		return "generic";
		
	}
	
}
