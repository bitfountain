package org.bitfountain.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel.MapMode;

/**
 * Read a file by fixed-size chunks.
 * 
 * @author Amos Wenger
 */
public class ChunkedFileReader {

	/** The byte buffer in which the file is read, by chunks. */
	MappedByteBuffer byteBuffer;
	
	/** The int buffer in which chunks are stored, for convenience. */
	private int[] intBuffer;
	
	/** EOF indicator. */
	private boolean hasNext = true;
	
	/** The input stream from which we read. */
	FileInputStream fileInputStream;
	
	/** Length of the file. */
	long length;
	
	/** Position in file. */
	long position;
	
	/** Size of the chunks to read. */
	private int chunkSize;
	
	/**
	 * Create a new chunked file reader.
	 * 
	 * @param pFileInputStream The input stream from which to read.
	 * @param pChunkSize Size of chunks to read.
	 * @throws IOException Any kind of I/O error occured.
	 */
	public ChunkedFileReader(final FileInputStream pFileInputStream,
			final int pChunkSize) throws IOException {
		this.chunkSize = pChunkSize;
		this.fileInputStream = pFileInputStream;
		intBuffer  = new int [pChunkSize];
		
		length = pFileInputStream.available();
		byteBuffer = pFileInputStream.getChannel().map(MapMode.READ_ONLY, 0, length);
	}
	
	/**
	 * @return read a chunk and return it. if we reach the end-of-file,
	 * the buffer will be padded with zeroes
	 */
	@SuppressWarnings("cast")
	public final int read() {
		if (position >= length) {
			hasNext = false;
			return -1;
		}
			
		int diff = (int) (length - position);
		int readSize = chunkSize;
		if (diff < chunkSize) {
			readSize = diff;
			for (int i = readSize; i < chunkSize; i++) {
				intBuffer[i] = 0;
			}
		}
		
		for (int i = 0; i < readSize; i++) {
			intBuffer[i] = ((int) byteBuffer.get()) + 128;
		}
		
		position += readSize;
		return readSize;
	}
	
	/**
	 * @return true if we still have data to read, false if we're at the
	 * end of the file.
	 */
	public final boolean hasNext() {
		return hasNext;
	}
	
	/**
	 * @return the int buffer in which we store chunks of data read from
	 * the file.
	 */
	public final int[] getBuffer() {
		return intBuffer;
	}
	
	/**
	 * Close the reader. 
	 * 
	 * @throws IOException In case any I/O error occurs.
	 */
	public final void close() throws IOException {
		fileInputStream.close();
	}
	
}
