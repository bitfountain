package org.bitfountain.io;

/**
 * Our own evil logger class with a lot fewer features than java's one.
 * But it feels good to be evil. 
 * 
 * @author Amos Wenger
 */
public interface Logger {

	/** @param message Log a message */
	void log(String message);
	
}
