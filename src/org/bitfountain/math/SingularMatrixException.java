package org.bitfountain.math;

/**
 * That exception is thrown when a matrix couldn't be reduced
 * with the Gauss algorithm.
 * 
 * @author Amos Wenger
 */
public class SingularMatrixException extends Exception {

	/** UID for serialiazation. */
	private static final long serialVersionUID = -4520960508443397993L;

	/**
	 * Create a new gauss exception with a message.
	 * @param message Message of the exception.
	 */
	public SingularMatrixException(final String message) {
		super(message);
	}
	
}
