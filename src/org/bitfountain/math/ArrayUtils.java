package org.bitfountain.math;

/**
 * Utility functions on arrays. 
 * 
 * @author Amos Wenger
 */
public final class ArrayUtils {

	/** Private constructor to avoid class instanciation. */
	private ArrayUtils() { }
	
	/**
	 * Check for presence of an int in an array.
	 * 
	 * @param array The array to search in
	 * @param i the number to find
	 * @param max the max index in which to search
	 * @return true if the array contains i, false otherwise
	 */
	public static boolean contains(final int[] array, final int i,
			final int max) {
		for (int j = 0; j < max; j++) {
			if (array[j] == i) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Copy a multi-dimensional array into another one.
	 * 
	 * @param source The source
	 * @param destination The destination
	 */
	public static void multiCopy(final int[][] source,
			final int[][] destination) {
		for (int a = 0; a < source.length; a++) {
			System.arraycopy(source[a], 0, destination[a], 0, source[a].length);
		}
	}

}
