package org.bitfountain.math.reedsolomon;

import org.bitfountain.math.Matrix;

/**
 * A GF(2ʷ) that supports w = 4, 8, or 16, does multiplication, division,
 * and computes the Vandermonde Matrix for a given number of checksum words
 * 
 * Sources:
 * 
 *  [1] Planck, James (1999). A Tutorial on Reed-Solomon Coding for Fault-Tolerance in RAID-like Systems
 *      http://www.cs.utk.edu/~plank/plank/papers/CS-96-332.pdf
 * 
 * @author Amos Wenger
 * @author Rivo Vasta
 */

public class GaloisField {

	/** Primitive polys for different for w = 4. The coefficients are in GF(2). */
	private static final int PRIM_POLY_4  = 023;     //                  x⁴            + x + 1
	
	/** Primitive polys for different for w = 8. The coefficients are in GF(2). */
	private static final int PRIM_POLY_8  = 0435;    //             x⁸ + x⁴ + x³ + x²      + 1
	
	/** Primitive polys for different for w = 16. The coefficients are in GF(2). */
	private static final int PRIM_POLY_16 = 0210013; // x¹⁶ + x¹²           + x³       + x + 1
	
	/** Discrete logarithms precomputed tables to speed up multiplication/division.
	 * 	"i" stands for "inverse"
	 */
	private short[] gflog, gfilog;
	
	/** Precomputed add and mul tables. */
	public int[][] mulTable, divTable;
	
	/** The exponent, e.g. 8 for F256 */
	private final int w;
	
	/** The dimension, e.g. 2⁸ */
	private final int dimension; 
	
	/**
 	 * @return the field's dimension
	 */
	public final int getDimension() {
		return dimension;
	}
	
	/**
	 * @return the exponent, e.g. 8 for 2⁸
	 */
	public final int getExponent() {
		return w;
	}
	
	/**
	 * Create a new field of dimension size 2ʷ.
	 * 
	 * @param pW the exponent for this field, e.g. 8 for F256
	 */
	public GaloisField(final int pW) {
		this.w = pW;
		this.dimension = 1 << pW; // 2ʷ
		setupTables();
		setupMulDivTables();
	}
	
	/**
	 * Precompute discrete logarithms in this field.
	 */
	private void setupTables() {
		int primPoly;
		switch(w) {
			case 4:  primPoly = PRIM_POLY_4;  break;
			case 8:  primPoly = PRIM_POLY_8;  break;
			case 16: primPoly = PRIM_POLY_16; break;
			default: throw new Error("Unsupported w = " + w + " for Galois field");
		}
		
		gflog =  new short[dimension];
		gfilog = new short[dimension];
		int b = 1;
		// multiplication is cyclic in a Galois field, we only need values of logarithms from 0 to dimension − 1
		for (int log = 0; log < dimension - 1; log++) {
			gflog [b]   = (short) log;
			gfilog[log] = (short) b;
			b = b << 1;
			// if b is of greater degree than the Galois field dimension..
			if ((b & dimension) != 0) {
				// b = b modulo primPoly
				b = b ^ primPoly;
			}
		}
	}
	
	/**
	 * Compute multiplication and division tables in this field.
	 */
	private void setupMulDivTables() {
		mulTable = new int[this.dimension][this.dimension];
		for (int i = 0; i < mulTable.length; i++) {
			for (int j = 0; j < mulTable.length; j++) {
				mulTable[i][j] = this.galoisMul(i, j);
			}
		}
		
		divTable = new int[this.dimension][this.dimension];
		for (int i = 0; i < divTable.length; i++) {
			for (int j = 1; j < divTable.length; j++) {
				divTable[i][j] = this.galoisDiv(i, j);
			}
		}
	}

	/**
	 * @return an n+m × n Vandermonde-like matrix.
	 * 
	 * @param m Number of check bytes
	 * @param n Number of data bytes
	 */
	public final Matrix getVandermondeMatrix(final int m, final int n) {
		int k = m + n;
		
		if (k >= getDimension()) {
			throw new ArithmeticException("Trying to get a " + k + "×" + n + " Vandermonde matrix in a 2^" + w + " space. What's wrong with you?");
		}
		Matrix matrix = new Matrix(k, n, this);
		
		for (int column = 1; column <= n; column++) {
			int j = 1;
			for (int row = 1; row <= k; row++) {
				matrix.data[row - 1][column - 1] = j;
				j = mul(j, column);
			}
		}
		
		matrix.gauss();
		
		return matrix;
	}
	
	/**
	 * Add a and b in this galois field.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the addition of a and b.
	 */
	public final int add(final int a, final int b) {
		// Ref. [1] p. 6
		// addition is XOR in 2ʷ fields
		return a ^ b;
	}
	
	/**
	 * Subtract b from a in this galois field.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the subtraction of b from a.
	 */
	public final int sub(final int a, final int b) {
		// Ref. [1] p. 6
		// subtraction is also XOR in 2ʷ fields
		return a ^ b;
	}
	
	/**
	 * Multiply a and b in this galois field, without resorting
	 * to multiplication tables.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the multiplication
	 */
	private int galoisMul(final int a, final int b) {
		// Ref. [1] pp. 6-7
		// a * b = ilog(log(a) + log(b))
		
		int sumLog;
		if (a == 0 || b == 0) {
			return 0;
		}
		sumLog = gflog[a] + gflog[b];
		if (sumLog >= dimension - 1) {
			sumLog -= dimension - 1;
		}
		return gfilog[sumLog];
	}
	
	/**
	 * Multiply a and b in this galois field.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the multiplication
	 */
	public final int mul(final int a, final int b) {
		return mulTable[a][b];
	}
	
	/**
	 * Divide a by b in this galois field, without resorting to
	 * division tables.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the division
	 */
	private int galoisDiv(final int a, final int b) {
		// Ref. [1] pp. 6-7
		// a / b = ilog(log(a) − log(b))
		int diffLog;
		if (a == 0) {
			return 0;
		}
		if (b == 0) {
			throw new ArithmeticException("Division by zero attempt!");
		}
		diffLog = gflog[a] - gflog[b];
		if (diffLog < 0) {
			diffLog += dimension - 1;
		}
		return gfilog[diffLog];
	}	
	
	/**
	 * Divide a by b in this galois field.
	 * 
	 * @param a first operand
	 * @param b second operand
	 * @return result of the division
	 */
	public final int div(final int a, final int b) {
		return divTable[a][b];
	}
	
}
