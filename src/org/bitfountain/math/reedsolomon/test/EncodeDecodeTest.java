package org.bitfountain.math.reedsolomon.test;

import java.io.File;
import java.io.IOException;

import org.bitfountain.math.SingularMatrixException;
import org.bitfountain.math.reedsolomon.decoding.RSDecoder;
import org.bitfountain.math.reedsolomon.encoding.RSEncoder;

/**
 * Test Reed-Solomon encoding and decoding. 
 * 
 * @author Amos Wenger
 */
public final class EncodeDecodeTest {

	/** Don't need a default constructor that is public. */
	private EncodeDecodeTest() { }
	
	/**
	 * Entry point of the test case.
	 * 
	 * @param args command line arguments.
	 * 
	 * @throws IOException In case anything goes wrong in the I/O.
	 * @throws SingularMatrixException In case too much data was lost
	 */
	public static void main(final String[] args) throws IOException, SingularMatrixException {
		
		RSEncoder encoder = new RSEncoder();
		RSDecoder decoder = new RSDecoder();
		
		String testDir = "/blue/Avatars/";
		File file = new File(testDir, "LightAtTheEndOfTheRoad.jpg");
		
		for (int i = 0; i < 5; i++) {
		
			// Step 1: encode
			encoder.encodeFile(file);
			
			// Step 2: mutilate & decode
			decoder.decodeFile(file);
			
		}
	}

}
