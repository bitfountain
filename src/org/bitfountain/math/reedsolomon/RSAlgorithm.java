package org.bitfountain.math.reedsolomon;

import org.bitfountain.math.Matrix;

/**
 * Base class for the Reed-Solomon encoding and decoding algorithm. 
 * 
 * @author Amos Wenger
 */
public abstract class RSAlgorithm {

	/** Default size of our Galois Field = 2⁸. */
	private static final int DEFAULT_GALOIS_FIELD_SIZE = 8;
	
	/** By default, use 223 data bytes. */
	public static final int DEFAULT_DATA_BLOCK_SIZE = 223;
	
	/** By default, use 32 checksum bytes. */
	public static final int DEFAULT_CHECKSUM_BLOCK_SIZE = 32;
	
	/** 223 + 32 = 255, all good. */
	public static final int DEFAULT_TOTAL_BLOCK_SIZE = DEFAULT_DATA_BLOCK_SIZE + DEFAULT_CHECKSUM_BLOCK_SIZE;

	/** the finite field in which we'll be computing in. */
	protected GaloisField field;
	
	/** the Vandermonde matrix used to produce codewords. */
	protected Matrix vm;
	
	/** total words. */
	protected int k;
	
	/** checksum words. */
	protected int m;
	
	/** data words. */
	protected int n;
	
	/**
	 * Construct a new RSAlgorithm, initialize the galois field.
	 */
	public RSAlgorithm() {
		field = new GaloisField(DEFAULT_GALOIS_FIELD_SIZE); // choose a 2⁸ = 256 field so we fall on the byte boundaries
		m = DEFAULT_CHECKSUM_BLOCK_SIZE;
		n = DEFAULT_DATA_BLOCK_SIZE;
		k = m + n;
		vm = field.getVandermondeMatrix(m, n);
	}
	
	/**
	 * @return number of bytes used to transmit data.
	 */
	public final int getDataBlockSize() {
		return n;
	}
	
	/**
	 * @return number of bytes used for checking.
	 */
	public final int getChecksumBlockSize() {
		return m;
	}
	
	/**
	 * @return total number of bytes in a block.
	 */
	public final int getTotalBlockSize() {
		return n + m;
	}
	
}
