package org.bitfountain.math.reedsolomon.decoding;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bitfountain.gui.client.ProgressListener;
import org.bitfountain.io.ChunkedFileReader;
import org.bitfountain.math.ArrayUtils;
import org.bitfountain.math.Matrix;
import org.bitfountain.math.SingularMatrixException;
import org.bitfountain.math.reedsolomon.RSAlgorithm;

/**
 * Reed-Solomon decoding implementation.
 * 
 * @author Amos Wenger
 */
public class RSDecoder extends RSAlgorithm {

	/** Number of lost bytes, can be adjusted for testing. */
	public static final int LOST_BYTES = 0;
	
	/**
	 * Decode Reed-Solomon-encoded data, and store the result into output.
	 * 
	 * @param input Data to be decoded
	 * @param lostIndices Indices that were lost during transmission
	 * @param output Decoded data.
	 * 
	 * @throws SingularMatrixException This exception is thrown if too much of the data
	 * has been lost, and we couldn't decode the data
	 */
	public final void decode(final int[] input, final int[] lostIndices, final int[] output) throws SingularMatrixException {
		
		int k = m + n;
		
		Matrix matrix = new Matrix(k, n + 1, field);
		ArrayUtils.multiCopy(vm.data, matrix.data);
		
		for (int row = 0; row < matrix.getRows(); row++) {
			if (ArrayUtils.contains(lostIndices, row, lostIndices.length)) {
				for (int col = 0; col < matrix.getCols(); col++) {
					matrix.data[row][col] = 0; 
				}
			} else {
				matrix.data[row][n] = input[row];
			}
		}
		
		matrix.gaussBuster(n);
		
		for (int row = 0; row < n; row++) {
			output[row] = matrix.data[row][n];
		}
		
	}
	
	/**
	 * Decode a Reed-Solomon encoded file.
	 * 
	 * @param file The file to be decoded
	 * @throws IOException If the file isn't found or something else goes wrong.
	 * @throws SingularMatrixException Too much data was lost to be recovered.
	 */
	public final void decodeFile(final File file) throws IOException, SingularMatrixException {
		decodeFile(file, null);
	}
	
	/**
	 * Decode a Reed-Solomon encoded file, with an optional
	 * progress listener.
	 * 
	 * @param file The file to be decoded
	 * @param listener The progress listener
	 * @throws IOException If the file isn't found or something else goes wrong.
	 * @throws SingularMatrixException Too much data was lost to be recovered.
	 */
	public final void decodeFile(final File file, final ProgressListener listener) throws IOException, SingularMatrixException {
		
		int blockSize = getDataBlockSize();
		
		long length = file.length();
		long t1 = System.nanoTime();
		ChunkedFileReader encodedReader = new ChunkedFileReader(
				new FileInputStream(file.getPath() + ".encoded"),
				getTotalBlockSize());
		FileOutputStream fileOutputStream = new FileOutputStream(
				file.getPath() + ".decoded");
		BufferedOutputStream decodedWriter = new BufferedOutputStream(
				fileOutputStream);
		int i = 0;
		long decodedBytes = 0;
		
		long last = System.nanoTime();
		
		while (encodedReader.hasNext()) {
			int count = encodedReader.read();
			if (count == -1) {
				break;
			}
			
			int[] encoded = encodedReader.getBuffer();
			int[] lostIndices = new int[LOST_BYTES];
			mutilate(encoded, lostIndices);
			int[] received = encoded;
	
			if (length < blockSize) {
				// last turn, blockSize is the remaining length =)
				blockSize = (int) length;
			} else {
				length -= blockSize;
			}
			int[] decoded = new int[getDataBlockSize()];
			decode(received, lostIndices, decoded);
			byte[] decodedByte = new byte[decoded.length];
			
			for (int j = 0; j < blockSize; j++) {
				decodedByte[j] = (byte) (decoded[j] - 128);
			}
			decodedWriter.write(decodedByte, 0, blockSize);
			i++;
			
			decodedBytes += blockSize;
			
			// We only redraw the UI every 50ms
			if (listener != null && (System.nanoTime() - last > 50e6
					|| blockSize < getDataBlockSize())) {
				last = System.nanoTime();
				listener.onProgress(decodedBytes, file.length(),
						(int) (decodedBytes * 100 / file.length()), 0);
			}
		}
		decodedWriter.close();
		fileOutputStream.close();
		encodedReader.close();
		long t2 = System.nanoTime();
		System.out.println("Decoding time = " + ((t2 - t1) / 1000000) + "ms");
	}

	/**
	 * Mutilate an int array, e.g. randomly remove up to lostIndices.length
	 * ints, and store their indices in the lostIndices array
	 * 
	 * @param input Input from which 
	 * @param lostIndices the indices of elements that have been removed
	 */
	private static void mutilate(final int[] input, final int[] lostIndices) {
		for (int i = 0; i < lostIndices.length; i++) {
			int value = -1;
			// generate a unique (ie. not in lostIndices yet) random value
			while (value == -1 || ArrayUtils.contains(lostIndices, value, i)) {
				value = (int) (Math.random() * (input.length - 1));
			}
			lostIndices[i] = value;
			input[value] = 0;
		}
	}

}
