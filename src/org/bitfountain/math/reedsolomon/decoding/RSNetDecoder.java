package org.bitfountain.math.reedsolomon.decoding;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bitfountain.math.ArrayUtils;
import org.bitfountain.math.Matrix;
import org.bitfountain.math.SingularMatrixException;
import org.bitfountain.math.reedsolomon.encoding.RSNetEncoder;
import org.bitfountain.network.Progressive;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FilePart;

/**
 * Reed-Solomon decoder adapted to receiving packets from the network.
 * 
 * @author Amos Wenger
 */
public class RSNetDecoder extends RSDecoder implements Progressive {

	/** We store BLOCK_SIZE blocks so we can deinterlace. */
	public byte[][] blocks = new byte[RSNetEncoder.PACKET_DATA_SIZE][k];
	
	/** Where we're saving the file to. */
	FileOutputStream outputStream;
	
	/** The file we're receiving. */
	FileHeader header;
	
	/** The decoded file. */
	File destination;
	
	/** Indices of the packets that were lost. */
	int[] lostIndices = new int[RSNetEncoder.PACKET_DATA_SIZE];
	
	/** Number of packets lost for a k-packets chunk. */
	int numLostIndices = 0;
	
	/** Index of the last packet received - used to know if there are losses. */
	int lastPacketNumber = 0;
	
	/** Total number of packets lost. */
	int lostPackets = 0;
	
	/** Bytes we've received so far. */
	long receivedSize = 0;
	
	/** Offset in the buffer. */
	int packetOffset = 0;
	
	/** true when the download is finished. */
	boolean isFinished = false;
	
	/** expected number of packets for this file. */
	long numPackets;
	
	/** time at which the decoding started. */
	long startTime;
	
	/** last speed evaluation, in Kibi/s. */
	double lastSpeed = 0.0;
	
	/**
	 * Encode a file and send it over an output stream.
	 * 
	 * @param pHeader Header of the file we want to receive.
	 * @param pDestination Where to save the decoded file.
	 * 
	 * @throws IOException In case there's an I/O error.
	 */
	public RSNetDecoder(final FileHeader pHeader, final File pDestination) throws IOException {
		
		this.header = pHeader;
		this.destination = pDestination;

		destination.getParentFile().mkdirs();
		outputStream = new FileOutputStream(pDestination);
		numPackets = pHeader.numPackets(n, k);
		
		startTime = System.currentTimeMillis();
	
	}
	
	/**
	 * Do a little bit more work from the received packet.
	 * 
	 * @param filePart The packet we just receivedSize
	 * 
	 * @return true if it wants more data, false if the file receive is complete.
	 * 
	 * @throws IOException We're writing to a file - maybe the earth has stopped?
	 * @throws SingularMatrixException In case we couldn't decode.
	 */
	public final boolean step(final FilePart filePart) throws IOException, SingularMatrixException {

		if (isFinished) {
			System.out.println("Is finished, returning false!");
			return false;
		}
		
		while (filePart.getPacketNumber() - 1 > lastPacketNumber && packetOffset < k) {
			// skip the lost packets in the buffer
			lostIndices[numLostIndices++] = packetOffset;
			packetOffset++;
			lastPacketNumber++;
			lostPackets++;
		}
		
		if (packetOffset >= k) {
			return processBuffer();
		}
		
		// we get one byte per block in each packet
		for (int row = 0; row < RSNetEncoder.PACKET_DATA_SIZE; row++) {
			blocks[row][packetOffset] = filePart.getBlockParts()[row];
		}
		
		// update our last packet number
		lastPacketNumber = filePart.getPacketNumber();
		
		// update our position in the packet buffer
		packetOffset++;
		
		if (packetOffset >= k) {
			return processBuffer();
		}
		
		return true;
		
	}

	/**
	 * When our buffer is full, we need to decode it and write the
	 * decoded data to the file.
	 * 
	 * @return true if we still need some more data, false if we're done here.
	 * 
	 * @throws IOException In case writing to the file goes wrong.
	 * @throws SingularMatrixException In case the data couldn't be decoded 
	 */
	@SuppressWarnings("cast")
	private boolean processBuffer() throws IOException, SingularMatrixException {
		
		byte[] outputBytes = new byte[k];
		
		Matrix matrix = new Matrix(k, n + RSNetEncoder.PACKET_DATA_SIZE, field);
		ArrayUtils.multiCopy(vm.data, matrix.data);
		
		// copy all the received data into the matrix
		for (int row = 0; row < k; row++) {
			if (ArrayUtils.contains(lostIndices, row, lostIndices.length)) {
				// lost indices? fill with 0.
				for (int col = 0; col < matrix.getCols(); col++) {
					matrix.data[row][col] = 0; 
				}
			} else {
				// for every block, copy the data
				for (int col = 0; col < RSNetEncoder.PACKET_DATA_SIZE; col++) {
					matrix.data[row][n + col] = ((int) blocks[col][row]) + 128;
				}	
			}
		}
		
		matrix.gaussBuster(n);
		
		for (int col = 0; col < RSNetEncoder.PACKET_DATA_SIZE; col++) {
			for (int row = 0; row < k; row++) {
				outputBytes[row] = (byte) (matrix.data[row][n + col] - 128);
			}
			
			int diff = (int) (header.getFileSize() - receivedSize);
			if (diff <= 0) {
				// we've reached the end of the file!
				outputStream.close();
				isFinished = true;
				return false;
			}
			
			int blockSize = diff < getDataBlockSize() ? diff : getDataBlockSize();
			receivedSize += blockSize;
			outputStream.write(outputBytes, 0, blockSize);
		}
		
		
		// reset offset in the buffer
		packetOffset = 0;
		
		// reset number of lost indices
		numLostIndices = 0;
		
		return true;
	}
	
	/**
	 * @return header info about the file we're receiving.
	 */
	public final FileHeader getHeader() {
		return header;
	}
	
	/**
	 * @return an estimation of the progress of the file reception, from 0.0 to 1.0
	 */
	public final double getProgress() {
		return (double) lastPacketNumber / (double) numPackets;
	}
	
	/**
	 * @return the destination — decoded file on the disk.
	 */
	public final File getDestination() {
		return destination;
	}

	@Override
	public final boolean isFinished() {
		return isFinished;
	}

	@Override
	public double getSpeed() {
		if (!isFinished()) {
			double diffSec = ((double) System.currentTimeMillis() - startTime) / 1000.0;
			lastSpeed = (getProgress() * header.getFileSize() / 1024.0 / diffSec);
		}
		
		return lastSpeed;
	}
	
}
