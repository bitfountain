package org.bitfountain.math.reedsolomon.encoding;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bitfountain.gui.client.ProgressListener;
import org.bitfountain.io.ChunkedFileReader;
import org.bitfountain.math.Vector;
import org.bitfountain.math.reedsolomon.RSAlgorithm;

/**
 * A Reed-Solomon encoder that works on files.
 * 
 * @author Amos Wenger
 */
public class RSEncoder extends RSAlgorithm {
	
	/**
	 * Encode a block. 
	 * 
	 * @param input data to be encoded
	 * @return encoded data
	 */
	public final int[] encode(final int[] input) {
		final int k = m + n;
		int[] output = new int[k];
		System.arraycopy(input, 0, output, 0, n);
		
		for (int row = n; row < k; row++) {
			int total = 0;
			for (int col = 0; col < n; col++) {
				total = field.add(total, field.mul(input[col], vm.data[row][col]));
			}
			output[row] = total;
		}
		
		return output;
	}
	
	/**
	 * Encode a whole file.
	 * 
	 * @param file file to be encoded
	 * 
	 * @throws IOException In case I/O goes awry.
	 */
	public final void encodeFile(final File file) throws IOException {
		encodeFile(file, null);
	}
	
	/**
	 * Encode a whole file, with an optional progress listener.
	 * 
	 * @param file file to be encoded
	 * @param listener progress listener
	 * 
	 * @throws IOException In case I/O doesn't go too well.
	 */
	public final void encodeFile(final File file, final ProgressListener listener) throws IOException {
		long t1 = System.nanoTime();
		ChunkedFileReader inputReader = new ChunkedFileReader(new FileInputStream(file.getPath()), getDataBlockSize());
		FileOutputStream fileOutputStream = new FileOutputStream(file.getPath() + ".encoded");
		BufferedOutputStream encodedWriter = new BufferedOutputStream(fileOutputStream);
		int i = 0;
		long encodedBytes = 0;
		
		long last = System.nanoTime();
		
		while (inputReader.hasNext()) {
			int count = inputReader.read();
			if (count == -1) {
				break;
			}
			
			int[] encoded = encode(inputReader.getBuffer());
			
			byte[] output = new byte[encoded.length];
			Vector.toByteArray(encoded, output);
			
			encodedWriter.write(output);
			
			i++;
			
			if ((file.length() - encodedBytes) > getDataBlockSize()) {
				encodedBytes += getDataBlockSize();
			} else {
				encodedBytes = file.length();
			}
			
			// We only redraw the UI every 50ms
			if (listener != null && (System.nanoTime() - last > 50e6 || encodedBytes == file.length())) {
				last = System.nanoTime();
				listener.onProgress(encodedBytes, file.length(), (int) (encodedBytes * 100 / file.length()), 0);
			}
		}
		
		encodedWriter.close();
		fileOutputStream.close();
		inputReader.close();
		long t2 = System.nanoTime();
		System.out.println("Encoding time = " + ((t2 - t1) / 1000000) + "ms");
	}
	
}
