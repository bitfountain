package org.bitfountain.math.reedsolomon.encoding;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

import org.bitfountain.io.ChunkedFileReader;
import org.bitfountain.math.Vector;
import org.bitfountain.network.Task;
import org.bitfountain.network.packets.FileHeader;
import org.bitfountain.network.packets.FilePart;

/**
 * Reed-Solomon encoder adapted to sending packets over a network.
 * 
 * @author Amos Wenger
 */
public class RSNetEncoder extends RSEncoder implements Task {
	
	/** Max packet size, in bytes. */
	public static final int MAX_PACKET_SIZE = 1400;
	
	/** How much data we can fit in a packet, in bytes. */
	public static final int PACKET_DATA_SIZE = MAX_PACKET_SIZE - FilePart.HEADER_SIZE;
	
	/** We store BLOCK_SIZE blocks so we can interlace. */
	public byte[][] blocks = new byte[PACKET_DATA_SIZE][getTotalBlockSize()];
	
	/** The file we're sending. */
	final File file;
	
	/** Header information about the file we're sending. */
	final FileHeader header;
	
	/** Allows us to read chunks of the file we're sending. */
	final ChunkedFileReader reader;
	
	/** Stream over which to send the encoded file. */
	final DataOutputStream stream;
	
	/** Position of the next packet to be send in the buffer. */
	int packetOffset = 0;
	
	/** Number of the packet we're about to send. */
	int packetNumber = 0;
	
	/** Number of already processed bytes. */
	long remainingBytes;
	
	/** Number of bytes in the buffer. */
	long bytesInBuffer;
	
	/** true when the download is finished. */
	boolean isFinished;
	
	/** time at which the decoding started. */
	long startTime;
	
	/** last speed evaluation, in Kibi/s. */
	double lastSpeed = 0.0;
	
	/**
	 * Encode a file and send it over an output stream.
	 * 
	 * @param pFile File to be sent.
	 * @param pStream Stream over which to send the encoded file.
	 * 
	 * @throws IOException In case there's an I/O error.
	 */
	public RSNetEncoder(final File pFile, final DataOutputStream pStream) throws IOException {
		
		file = pFile;
		stream = pStream;
		
		remainingBytes = file.length();
		
		// Write the header
		header = new FileHeader(file.getName(), file.length());
		header.write(stream);
		stream.flush();
		
		startTime = System.currentTimeMillis();
		
		// Read the file, and encode it into several blocks
		reader = new ChunkedFileReader(new FileInputStream(file), getDataBlockSize());
		
		// initial refill
		refillBuffer();
		
	}
	
	/**
	 * Send one more block of the encoded data through the network.
	 * 
	 * @return true if we still have work to do, false otherwise.
	 * 
	 * @throws IOException In case there's a network problem.
	 */
	public final boolean step() throws IOException {
		
		// make sure the buffer is filled
		if (packetOffset >= getTotalBlockSize()) {
			if (!refillBuffer()) {
				// if we couldn't refill our buffer, we're at EOF - return!
				isFinished = true;
				return false;
			}
		}
			
		if (packetOffset < getTotalBlockSize()) {
			ByteArrayOutputStream packet = new ByteArrayOutputStream(PACKET_DATA_SIZE);
			DataOutputStream dataStream = new DataOutputStream(packet);
			
			// we add one byte per block in the packet
			for (int row = 0; row < PACKET_DATA_SIZE; row++) {
				dataStream.write(blocks[row][packetOffset]);
			}
			
			// send the packet
			new FilePart(header, packetNumber, packet.toByteArray()).write(stream);
			
			// we keep our packets numbered so that the server can deinterlace
			// even if a few packets are lost
			packetNumber++;
			
			// go through the whole buffer
			packetOffset++;
		}
		
		return true;
		
	}

	/**
	 * Refill the buffer with encoded blocks.
	 * 
	 * @return true if we have read some data, false if we're at the end of the file.
	 */
	private boolean refillBuffer() {
		
		// read till the end of the file
		if (!reader.hasNext()) {
			remainingBytes = 0;
			return false;
		}
		
		remainingBytes -= bytesInBuffer;
		bytesInBuffer = 0;
		int blockIndex = 0;
		
		// clear the buffer
		for (int row = 0; row < PACKET_DATA_SIZE; row++) {
			Arrays.fill(blocks[row], (byte) 0);
		}
		
		// fill the blocks buffer
		while (blockIndex < PACKET_DATA_SIZE) {
			byte[] output = new byte[getTotalBlockSize()];
			
			// read a block
			int readBytes = reader.read();
			if (readBytes != -1) {
				bytesInBuffer += readBytes;
				int[] input = reader.getBuffer();
			
				// encode
				int[] encoded = encode(input);
			
				// convert our int array of numbers to a byte array ready to be sent over the network
				Vector.toByteArray(encoded, output);
			}
			
			// add the block to the buffer
			blocks[blockIndex++] = output;
		}
		
		// reset packet offset now that the buffer is full again
		packetOffset = 0;
		
		return true;
		
	}

	@Override
	public final double getProgress() {
		long realRemainingBytes = remainingBytes - (long) (((double) packetOffset
				/ (double) getTotalBlockSize()) * bytesInBuffer);
		
		//System.out.println("remainingBytes = " + remainingBytes + ", bytesInBuffer = "
		//		+ bytesInBuffer + ", packetOffset = " + packetOffset
		//		+ ", realRemainingBytes = " + realRemainingBytes
		//		+ ", ratio = " + ((double) packetOffset / (double) getTotalBlockSize()));
		
		return 1.0 - ((double) realRemainingBytes / file.length());
	}

	/**
	 * @return header info about the file we're receiving.
	 */
	public final FileHeader getHeader() {
		return header;
	}
	
	@Override
	public final boolean isFinished() {
		return isFinished;
	}
	
	@Override
	public final String toString() {
		return "Sending file " + file.getName() + " to server";
	}

	@Override
	public double getSpeed() {
		if (!isFinished()) {
			double diffSec = ((double) System.currentTimeMillis() - startTime) / 1000.0;
			lastSpeed = (getProgress() * header.getFileSize() / 1024.0 / diffSec);
		}
		
		return lastSpeed;
	}
	
}
