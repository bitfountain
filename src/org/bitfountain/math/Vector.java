package org.bitfountain.math;

import java.util.Arrays;

/**
 * Store an array of int. 
 * 
 * @author Amos Wenger
 * @author Rivo Vasta
 */
public class Vector {

	/** Underlying array. */
	public int[] data;
	
	/**
	 * Create a new vector or a specified size.
	 * 
	 * @param size The size of the vector to create.
	 */
	public Vector(final int size) {
		data = new int[size];
	}
	
	/**
	 * Create a vector from an array of ints.
	 * 
	 * @param pData The array of ints from which to create the vector 
	 */
	public Vector(final int[] pData) {
		data = Arrays.copyOf(pData, pData.length);
	}
	
	/**
	 * Initializes a vector from a slice of an array.
	 * 
	 * @param input byte array
	 * @param start start index
	 * @param n size
	 */
	public Vector(final byte[] input, final int start, final int n) {
		this(n);
		int inputIndex = start;
		for (int outputIndex = 0; outputIndex < n; outputIndex++) {
			if (inputIndex >= input.length) {
				break; // the rest is zeroes.
			}
			data[outputIndex] = (input[inputIndex++] + 128);
		}
	}
	
	/**
	 * @return this vector as a byte array.
	 */
	public final byte[] toByteArray() {
		byte[] array = new byte[data.length];
		toByteArray(array);
		return array;
	}
	
	/**
	 * Writes this vector as a byte array into output.
	 * 
	 * @param output Buffer into which this vector will be written.
	 */
	public final void toByteArray(final byte[] output) {
		Vector.toByteArray(data, output);
	}

	/**
	 * Writes an int array into a byte array into output.
	 * 
	 * @param src The int array we read from
	 * @param dst Buffer into which this vector will be written.
	 */
	public static void toByteArray(final int[] src, final byte[] dst) {
		for (int k = 0; k < src.length; k++) {
			dst[k] = (byte) (src[k] - 128);
		}
	}

	/**
	 * @return The size of this array.
	 */
	public final int size() {
		return data.length;
	}
	
	/**
	 * Copies this column-vector into a row-greater matrix.
	 * @param destination the destination matrix which you want to copy this vector in
	 * @param row the row where this vector must start being copied
	 * @param column the column where this vector will be copied
	 */
	public final void copyInto(final Matrix destination, final int row, final int column) {
		int thisSize = this.size();
		
		for (int i = 0; i < thisSize; i++) {
			destination.data[i + row][column] = data[i];
		}
	}
	
	@Override
	public final String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('(');
		boolean isFirst = true;
		for (int i = 0; i < data.length; i++) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append(data[i]);
		}
		sb.append(')');
		return sb.toString();
	}
	
}
