package org.bitfountain.math;

import org.bitfountain.math.reedsolomon.GaloisField;

/**
 * A simple 2D matrix, on which we do all kind of operations
 * in a given Galois Field.
 * 
 * @author Amos Wenger
 * @author Rivo Vasta
 */
public class Matrix {
	
	/** List of rows, address like this: data[row][column]. */
	public int[][] data;
	
	/** The field in which we do all operations. */
	GaloisField galoisField;
	
	/**
	 * Creates a new Matrix.
	 * @param rows number of rows
	 * @param cols number of columns
	 * @param pGaloisField the Galois field the computations are made into
	 */
	public Matrix(final int rows, final int cols,
			final GaloisField pGaloisField) {
		data = new int[rows][cols];
		this.galoisField = pGaloisField;
	}
	
	/**
	 * Add the source line to the destination one.
	 * 
	 * @param source the line you wish to add to another
	 * @param destination the line being added
	 */
	@SuppressWarnings("unused")
	private void lineAdd(final int source, final int destination) {
		for (int i = 0; i < this.getCols(); i++) {
			this.data[destination][i] = galoisField.add(this.data[destination][i], this.data[source][i]);
		}
	}
	
	/**
	 * Subtract the destination line with the source one.
	 * (destination = destination − source)
	 * 
	 * @param destination the line to be subtracted
	 * @param source the line subtracting
	 */
	@SuppressWarnings("unused")
	private void lineSub(final int destination, final int source) {
		for (int i = 0; i < this.getCols(); i++) {
			this.data[destination][i] =
			   galoisField.sub(this.data[destination][i], this.data[source][i]);
		}
	}
	
	/**
	 * Subtracting n times the source line to the destination one.
	 * 
	 * @param destination the line to be subtracted
	 * @param factor times the source line
	 * @param source the line you wish to subtract n times to another one
	 */
	private void lineMulSub(final int destination,
			final int factor, final int source) {
		for (int i = 0; i < this.getCols(); i++) {
			data[destination][i] ^= galoisField.mulTable[factor][data[source][i]]; 
		}
	}
	
	/**
	 * Multiply n times the row line.
	 * 
	 * @param factor times
	 * @param row Which row to muliply
	 */
	@SuppressWarnings("unused")
	private void lineMul(final int factor, final int row) {
		for (int i = 0; i < getCols(); i++) {
			this.data[row][i] = galoisField.mul(factor, this.data[row][i]);
		}
	}
	
	/**
	 * Divide a line by some number.
	 * 
	 * @param row The line to divide
	 * @param divisor Number to divide the line in
	 */
	private void lineDiv(final int row, final int divisor) {
		for (int i = 0; i < this.getCols(); i++) {
			this.data[row][i] = galoisField.div(this.data[row][i], divisor);
		}
	}
	
	/**
	 * Swap two rows.
	 * 
	 * @param row1 First row
	 * @param row2 Second row
	 */
	private void rowSwap(final int row1, final int row2) {
		int[] temp = new int[this.getCols()];
		for (int col = 0; col < this.getCols(); col++) {
			temp[col] = this.data[row1][col];
			this.data[row1][col] = this.data[row2][col];
			this.data[row2][col] = temp[col];
		}
	}
	
	/**
	 * Naive implementation of Gauss reduction.
	 */
	public final void gauss() {
		
		// First step: normalize the vectors of the row space of the matrix
		final boolean debug = false;
		
		int realSize = this.getCols() < this.getRows() ? this.getCols() : this.getRows();
		for (int row = 0; row < realSize; row++) {
			
			// Divide the current line by aii (data[row][row])
			if (this.data[row][row] == 0) {
				int candidateRow = realSize - 1;
				while (data[candidateRow][row] == 0) {
					candidateRow--;
					//System.out.println("candidateRow = "+candidateRow+", row = "+row+", data[candidateRow][row] = "+data[candidateRow][row]);
				}
				this.rowSwap(row, candidateRow);
				row--;
			} else {
				lineDiv(row, this.data[row][row]);
				if (debug) {
					System.out.println("Step 1 – Intermediate result after line division : \n" + toString());
				}
				
				// Then substract the lines under with ai, row times the current one
				for (int i = row + 1; i < realSize; i++) {
					this.lineMulSub(i, this.data[i][row], row);
				}
				if (debug) {
					System.out.println("Step 1 – Intermediate result after line subtraction : \n" + toString());
				}
			}
		}
		
		// Second step: end with an [I G] matrix, with I on dimension row×row and G of dimension row×(cols−row)
		int row = Math.min(this.getRows() - 1, this.getCols() - 1);
		while (row > 0) {
			for (int i = row - 1; i >= 0; i--) {
				this.lineMulSub(i, this.data[i][row], row);
			}
			if (debug) {
				System.out.println("Step 2 – Intermediate result after line subtraction : \n" + toString());
			}
			row--;
		}
	}
	
	/**
	 * Specialized gauss algorithm for matrices that are almost identity.
	 * 
	 * Its behavior is documented in the report accompanying this project.
	 * 
	 * @param identitySize Size of the quasi-identity matrix inside this matrix.
	 * 
	 * @throws SingularMatrixException In case we can't reduce this matrix with gauss. 
	 */
	public final void gaussBuster(final int identitySize) throws SingularMatrixException {
		
		// the rows below the quasi-identity matrix that we have already swapped with all-zero lines 
		boolean[] takenCandidates = new boolean[getRows() - identitySize];
		//System.out.println("We have max "+takenCandidates.length+" candidates for swapping.");
		
		// the all-zero rows in the quasi-identity matrix that we have already swapped with non-zero lines
		int[] lostRows = new int[getRows() - identitySize];
		
		// temporary line used to anticipate the 1st pass, to evaluate candidate row
		// (just a copy of the actual line on which we do the operations)
		int[] tmpRow = new int[getCols()];
		
		// the number of non-null elements in lostRows
		int lostRowsSize = 0;
		
		for (int row = 0; row < identitySize; row++) {
			// we stumbled upon a null line!
			// (because the upper part of the matrix is supposed to be like
			// the identity matrix, if [row][row] is null, the whole line is)
			if (data[row][row] != 0) { continue; }
				
			// search a non-null line.
			int indicator = 0;
			
			// we search in the lower part of the matrix
			int candidateRow = identitySize;
			outer: while (true) {
				
				// already taken? skip.
				while (takenCandidates[candidateRow - identitySize]) {
					candidateRow++;
					
					if (candidateRow >= getRows()) {
						throw new SingularMatrixException("Matrix is singular!");
					}
				}
				
				// copy the candidate row into our temporary row to apply the computations without side effect on the real matrix
				System.arraycopy(data[candidateRow], 0, tmpRow, 0, getCols());
				
				for (int i = 0; i < lostRowsSize; i++) {
					int lostRow = lostRows[i];

					int factor = tmpRow[lostRow];
					for (int j = lostRow; j < getCols(); j++) {
						tmpRow[j] ^= galoisField.mulTable[factor][data[lostRow][j]];
					}
				}
				indicator = tmpRow[row];
				
				// if it's non-null, then the row is like we want
				if (indicator != 0) {
					break outer;
				}
				
				// otherwise, continue searching
				candidateRow++;
				
				if (candidateRow >= getRows()) {
					throw new ArithmeticException("Matrix is singular\n" + this);
				}
				
			}
			takenCandidates[candidateRow - identitySize] = true;
			
			rowSwap(row, candidateRow);
			
			// Gaussing left part of the swapped line
			for (int row2 = 0; row2 < row; row2++) {
				lineMulSub(row, data[row][row2], row2);
			}
			lineDiv(row, data[row][row]);
			
			lostRows[lostRowsSize++] = row;
		}
		
		for (int i = 0; i < lostRowsSize; i++) { 			
			int lostRow = lostRows[i];
			// eliminating non-zeroes on the right of the diagonal
			for (int col = lostRow + 1; col < identitySize; col++) {
				if (data[lostRow][col] != 0) {
					lineMulSub(lostRow, data[lostRow][col], col);
				}
			}
		}
		
	}
	
	/**
	 * Multiply this matrix with a vector.
	 * 
	 * @param input The vector with which to multiply
	 * @return The result of the multiplication with this vector.
	 */
	public final Vector mul(final Vector input) {
		Vector result = new Vector(getRows());
		for (int row = 0; row < getRows(); row++) {
			for (int column = 0; column < getCols(); column++) {
				result.data[row] = galoisField.add(result.data[row],
						galoisField.mul(input.data[column], data[row][column]));
			}
		}
		return result;
	}
	
	/**
	 * Multiply a vector with this matrix.
	 * 
	 * @param input Vector to multiply
	 * @param output Vector in which to store the result
	 */
	public final void mul(final int[] input, final int[] output) {
		for (int row = 0; row < getRows(); row++) {
			for (int column = 0; column < getCols(); column++) {
				output[row] = galoisField.add(output[row],
						galoisField.mul(input[column], data[row][column]));
			}
		}
	}
	
	/**
	 * @return number of columns
	 */
	public final int getCols() {
		return data[0].length;
	}
	
	/**
	 * @return number of rows
	 */
	public final int getRows() {
		return data.length;
	}

	/**
	 * Copies a matrix into another greater one.
	 * 
	 * @param destination the destination matrix
	 * @param rowIndex the line where you want to copy your matrix from
	 * @param colIndex the column where you want to copy your matrix from
	 */
	public final void copyInto(final Matrix destination,
			final int rowIndex, final int colIndex) {
		int thisRows = this.getRows();
		int thisCols = this.getCols();
		
		for (int i = 0; i < thisRows; i++) {
			for (int j = 0; j < thisCols; j++) {
				destination.data[i + rowIndex][j + colIndex] = this.data[i][j];
			}
		}
	}
	
	/**
	 * Copies a row from a matrix into another one.
	 * 
	 * @param sourceRow the row number of the source matrix
	 * @param destination the destination matrix
	 * @param row the row destination
	 * @param column the column destination
	 */
	public final void copyRowInto(final int sourceRow, final Matrix destination,
			final int row, final int column) {
		int thisCols = this.getCols();
		
		for (int j = 0; j < thisCols; j++) {
			destination.data[row][j + column] = this.data[sourceRow][j];
		}
	}
	
	@SuppressWarnings("boxing")
	@Override
	public final String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("    ");
		for (int column = 0; column < getCols(); column++) {
			sb.append(String.format(" %3d,", column));
		}	
		sb.append("\n");
		sb.append("^^^^");
		for (int column = 0; column < getCols(); column++) {
			sb.append(String.format("^^^^^", column));
		}	
		sb.append("\n");
		
		for (int row = 0; row < getRows(); row++) {
			sb.append(String.format("%3d ", row));
			sb.append('(');
			boolean isFirst = true;
			for (int column = 0; column < getCols(); column++) {
				if (isFirst) {
					isFirst = false;
				} else {
					sb.append(", ");
				}
				sb.append(String.format("%3d", data[row][column]));
			}	
			sb.append(")\n");
		}
		return sb.toString();
	}
	
}
