Example 22, p.20 of channelcoding_en.pdf

choose blocklength n between 1 and 256 = |F256|
n = 255
k = 224

dmin = n - k + 1 = 32

rate:
r =  224/256 = 0,8 ~= 7/8

a is a generator of the field,
a = x^8 + x^4 + x^3 + x + 1

Canonical RS code:

Find the set of all polynomials A(x) of degree at most k - 1 = 223